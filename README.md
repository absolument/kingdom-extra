Crawler outils de propection de données et statistiques

DataMTKingdom directory contains the original assets from kingdom.muxxu.com
They are protected by licence  CC-By-SA-NC

Le répertoire DataMTKingdom regroupe les assets qui viennent du site kingdom.muxxu.com
Ils sont protégés par une licence  CC-By-SA-NC


Keats directory contains Kingdom prototypes in Python and PHP languages.


KingdomCommerceSimulator contains sources of a trade simulator (HTML/JS).


MapGenerator contains a stand alone map generator for Kingdom, in Python language.
It is protected by licence GNU GPL v3.

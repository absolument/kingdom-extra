from random import randint
from pprint import pprint


R_link = {'Ble': ['Ferme', 'Paysan', 6], 'Bois': ['Cabane', 'Bucheron', 5],
          'Or': ['Marche', 'Marchand', 6], 'Marteau': ['Atelier', 'Ouvrier', 1],
          'Or_Ble': ['Chantier', 'Ouvrier', 1]}  # ressource : [batiment, metier]

population = {'Paysan': 1, 'Bucheron': 0, 'Ouvrier': 0,
              'Marchand': 0, 'Recruteur': 0, 'Citoyen': 0}

ressources = {'Ble': 30, 'Or': 0, 'Bois': 0,
              'Minerais': 0, 'Lin': 0, 'Chevaux': 0}

batiments = {'Palais': 1, 'Ferme': 0, 'Grenier': 0,
             'Marche': 0, 'Cabane': 0, 'Chantier': 0,
             'Caserne': 0, 'Mur': 0, 'Quartier_general': 0,
             'Atelier': 0, 'Tour_de_garde': 0, 'Academie_militaire': 0,
             'Archerie': 0, 'Manufacture': 0, 'Etables': 0,
             'Boucherie': 0, 'Forge': 0, 'Chauderon': 0}


def alignements(grille):
        G = list(grille)
        A = []
        for x in range(8):
            for y in range(len(G[x])):
                
                a = [(x,y)]  #Cases dans l'alignements
                for dy in range(1, 3): #Distance entre la case x,y+dy et x,y
                    if y+dy in range(len(G[x])) and (x,y+dy-1) in a and G[x][y+dy] == G[x][y] and G[x][y] != 0:
                        a.append((x, y+dy))
                    if y-dy in range(len(G[x])) and (x,y-dy+1) in a and G[x][y-dy] == G[x][y] and G[x][y] != 0:
                        a.append((x, y-dy))                           
                if len(a) >= 3 and sorted(a) not in A:  #Si il y a plus de 3 ressources alignées (pas compter 3 fois l'alignement)
                    A.append(a)
                    
                a = [(x,y)]
                for dx in range(1, 3):
                    if x+dx in range(len(G)) and (x+dx-1,y) in a and G[x+dx][y] == G[x][y] and G[x][y] != 0:
                        a.append((x+dx, y))
                    if x-dx in range(len(G)) and (x-dx+1,y) in a and G[x-dx][y] == G[x][y] and G[x][y] != 0:
                        a.append((x-dx, y))                           
                if len(a) >= 3 and sorted(a) not in A:
                    A.append(a)
        
        return sorted(A, reverse=True) # On renvoie les alignements de bas en haut


def gen_ressource():
        if ressources['Or'] > 0.5*sum(population.values())**2:  #Proba d'épée en fonction de l'or
            Epee = 10
        else:
            Epee = 3
        if ressources['Ble'] > sum(population.values())**2:   #Proba de citoyen en fonction du blé
            Citoyen = 13
        else:
            Citoyen = 2
        Ble = (100-(Epee+Citoyen))//4
        Bois = (100-(Epee+Citoyen))//4
        Or = (100-(Epee+Citoyen))//4
        Tour = (100-(Epee+Citoyen))//8
        Marteau = (100-(Epee+Citoyen+Ble+Bois+Or+Tour))
        R = ['Ble']*Ble + ['Bois']*Bois + ['Or']*Or + ['Marteau']*Marteau + ['Tour']*Tour + ['Epee']*Epee + ['Citoyen']*Citoyen        
        i = randint(0, 99)
        return R[i]


def gen_grille(show = False):
    G = [[0 for j in range(8)] for i in range(8)]  #On génère une grille pleine de 0=vide
    for x in range(8):
        for y in range(8):
            G[x][y] = gen_ressource().center(7)
            while len(alignements(G)) > 0:  #Tant que ce que la ressources créée génère un alignement, on change de ressource
                G[x][y] = gen_ressource().center(7)
                       
    if show :
        for i in range(8):
            print(G[i])               
    return G        


def coups_possibles(grille):
        G = list(grille)
        P = []
        for x in range(8):  # On parcourt les cases de la grille
            for y in range(8):
                # On parcourt les 4 cases autour de cette case
                for x2 in range(max(0, x-1), min(x+1, 7)+1):
                    for y2 in range(max(0, y-1+abs(x2-x)), min(y+1-abs(x2-x), 7)+1):
                        if (x2, y2) != (x, y):
                            A = sorted([(x, y), (x2, y2)])
                            if A not in P:                              
                                G[x][y], G[x2][y2] = G[x2][y2], G[x][y] # On simule l'échange
                                if len(alignements(G)) > 0:
                                    P.append([(x, y), (x2, y2)])                       
                                G[x][y], G[x2][y2] = G[x2][y2], G[x][y] # On les remet comme avant
        return P
                

                
def switch(G, case1, case2, show=False):
    x1,y1 = case1
    x2,y2 = case2    
    G[x1][y1], G[x2][y2] = G[x2][y2], G[x1][y1]
    A = alignements(G)
    E = [list(G)]
    prod = {'Ble': 0, 'Or': 0, 'Bois': 0, 'Tour': 0,
                'Marteau': 0, 'Epee': 0, 'Citoyen': 0}
   
    while len(A) > 0: 
        
        V = []  # Alignements valides 
        for i in range(len(A)):  # On parcourt les alignements 
            if len(V) > 0:    
                yin = sum(int(y in [c[1] for a in V for c in a]) for y in [c2[1] for c2 in A[i]]) > 0
                yout = sum(int(y not in [c[1] for a in V for c in a]) for y in [c2[1] for c2 in A[i]]) > 0
                xin = sum(int(x in [c[0] for a in V for c in a]) for x in [c2[0] for c2 in A[i]]) > 0
                xout = sum(int(x not in [c[0] for a in V for c in a]) for x in [c2[0] for c2 in A[i]]) > 0
            else:
                yin, yout, xin, xout = False, False, False, False              
            if not (yin and yout) and not (xin and xout):  # Si l'alignement est à coté ou pil au dessus
                V.append(A[i])  # On note les alignements valides
                R = G[A[i][0][0]][A[i][0][1]].replace(' ', '') # On enlève les espaces et on ajoute à la prod
                if R == 'Tour':
                    prod[R] += len(A[i]) - 2
                elif R == 'Epee':
                    prod[R] += population['Recruteur'] + len(A[i]) - 2
                elif R == 'Citoyen':
                    prod[R] += 1
                else:
                    prod[R] += R_link[R][2] * population[R_link[R][1]] * (1+0.2*batiments[R_link[R][0]]) * 0.25*(1+len(A[i]))
                print(R)
                for x, y in A[i]:
                    G[x][y] = '       ' # On fait disparaitre la ressource
            
            E.append(list(G))
                
            V.sort(reverse=True) #On parcourt de haut en bas maintenant
            if show:
                for i in range(8):
                    print(G[i])      
                print('\n')
                
            for i in range(len(V)):  # On fait tomber les ressources au dessus              
                xmin = min(c[0] for c in V[i])
                xmax = max(c[0] for c in V[i])
                for y in list(dict.fromkeys([c[1] for c in V[i]])): #on enlève les doublons
                    for xh in range(xmax, xmax-xmin, -1):
                        G[xh][y] = G[xh-(xmax-xmin+1)][y]
                    for xh in range(xmax-xmin, -1, -1):
                        G[xh][y] = gen_ressource().center(7)
                            
                if show:
                    for i in range(8):
                        print(G[i])
                    print('\n')
        A = alignements(G)  # On regarde si il y a à nouveau des alignements
        E.append(list(G))
    return E, prod
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
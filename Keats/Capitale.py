from Class_Unite import *
from Fonctions import *
from Grille import *
from random import randint
from pprint import pprint

R_link = {'Ble': ['Ferme', 'Paysan', 6], 'Bois': ['Cabane', 'Bucheron', 5],
          'Or': ['Marche', 'Marchand', 6], 'Marteau': ['Atelier', 'Ouvrier', 1],
          'Or_Ble': ['Chantier', 'Ouvrier', 1]}  # ressource : [batiment, metier]


class Joueur:
    def __init__(self, nom):
        self.nom = nom
        self.titre = 'Chevalier'
        self.age = 20
        self.sante = 'Excellente'
        self.gloire = 1
        self.apogee = 1
        self.commerce = 0   #Commerce total
        self.generaux = []
        self.capitale = None
        self.suzerain = None
        self.vassaux = []
        self.territoires = []    #Sa capitale s'ajoute lorsque'il s'installe dedans (pas fonctionnel à refaire) 
        
    def reset(self):        #Lorsque le joueur se fait vassaliser
        for v in self.vassaux :
            v.suzerain = None      #On libère les vassaux
        for t in self.territoires:
            for u in t.armee:
                u.barbare = True    #Les unités deviennent barbares
        self.territoires = [self.capitale]



class Lieu :
    def __init__(self, nom, x, y, voisins={}):
        self.nom = nom
        self.loc = x,y #Position sur la carte (à relier avec le taff de Talsi ?)
        
        self.voisins = voisins #Ville : temps
        for v in self.voisins.keys():
            graph.add_edge(self, v, 1)  #Construction du graphe pour déduire la distance (à modifier en fonction de Talsi)
        
        self.suzerain = None    #Lieu sous l'autoritéde "suzerain"
        self.armee = []
        self.generaux = []      #Généraux présents sur le lieu
        self.bataille = None #True or False, le lieu est-il en battaille ou non ?
        self.pos = 'Defense'
        
        self.distance = 0
        self.commerce = 0
        self.sc = 0     #Sous compteur pour le calcul du commerce
        
    def is_connected(self, capitale):  #Récursif, y a t-il plus opti ?
        res = False
        if self in capitale.joueur.territoires and capitale in self.voisins.keys():
            res = True
        else:
            for v in self.voisins.keys():
                if v in capitale.joueur.territoires and v.is_connected(capitale):
                    res = True
        return res
              
    
    def maj_commerce(self): 
        if self.suzerain:
            c = self.commerce
            if self.commerce < 0: 
                if len(self.armee) > 0:
                    self.sc += len(self.armee)
                    while self.sc >= 10*abs(c):
                        self.sc -= 10*abs(c)
                        self.commerce += 1
                    return
                else:
                    self.sc -= 2*(len(self.armee) - self.commerce//2)
                    while self.sc < 0 and c != 0:
                        self.sc += 10*abs(c)
                        self.commerce = max(self.commerce-1, -2*self.distance**2)
                    return 
        
            if self.commerce >= 0:
                if len(self.armee) > self.commerce//2:
                    self.sc += len(self.armee) - self.commerce//2
                    while self.sc >= 10*(c+1):
                        self.sc -= 10*(c+1)
                        self.commerce += 1
                    return
                else:
                    self.sc -= (len(self.armee) - self.commerce//2)**2
                    while self.sc < 0 and c != 0:
                        self.sc += 10*abs(c)
                        self.commerce -= 1
                    return 
            if not self.is_connected(self.suzerain.capitale):
                self.commerce = min(self.commerce, 0)

     
class Capitale(Lieu):
    def __init__(self, nom, x, y, voisins ={}, joueur=None):
        Lieu.__init__(self, nom, x, y, voisins)
        self.joueur = joueur
        if self.joueur:  #Si la capitale est occupée par un joueur, mal fait, le if ne doit pas être dans le init
            self.joueur.capitale = self
            self.joueur.territoires.append(self)
            self.batiments = {'Palais': 1, 'Ferme': 0, 'Grenier': 0,
                              'Marche': 0, 'Cabane': 0, 'Chantier': 0,
                              'Caserne': 0, 'Mur': 0, 'Quartier_general': 0,
                              'Atelier': 0, 'Tour_de_garde': 0, 'Academie_militaire': 0,
                              'Archerie': 0, 'Manufacture': 0, 'Etables': 0,
                              'Boucherie': 0, 'Forge': 0, 'Chauderon': 0} #Niveau 0 = Pas construit
            
            self.population = {'Paysan': 1, 'Bucheron': 0, 'Ouvrier': 0,
                               'Marchand': 0, 'Recruteur': 0, 'Citoyen': 0}
            self.ressources = {'Ble': 30, 'Or': 0, 'Bois': 0,
                               'Minerais': 0, 'Lin': 0, 'Chevaux': 0}
            
            batiments, ressources, population = self.batiments, self.ressources, self.population #variables utilisées par gen_grille()
            self.grille = gen_grille()  # importée depuis Grille.py

            self.garnison = {}
            self.tours_dispo = 100



    def tour(self, case1, case2, show=False):
        self.tours_dispo -= 1
        batiments, ressources, population = self.batiments, self.ressources, self.population #On met à jour les variables
        E, prod = switch(self.grille, case1, case2) #E = Liste des grilles à chaque étape
    
        self.maj_ressources()
        
        '''MAJ DU COMMERCE'''
        for t in self.joueur.territoires:
            t.maj_commerce()  
        if len(self.joueur.territoires) > 1 :
            self.joueur.commerce = sum(l.commerce for l in self.joueur.territoires[1:])
        
        if self.coups_possibles() == []:
            print('Regénération de le grille')
            self.gen_grille()
            
            
    def maj_ressources(prod):
        for r,p in prod.items(): 
            if r == 'Citoyen':
                self.population['Citoyen'] += 1
            elif r == 'Epee':
                for i in range(p):
                    self.armee.append(Soldat(self.joueur, self))
            elif r == 'Tour':
                self.tours_dispo += prod[r]  #On rajoute des tours
            elif r == 'Marteau':
                pass        #Pas encore géré la construction de batiment 
            else:
                self.ressources[r] += p             
        C = 0
        for g in self.joueur.generaux:
            C += max(0, len(g.armee) - g.reputation)
        for t in self.joueur.territoires:
            C += len(t.armee) - t.commerce         
        cons = {'Ble': sum(self.population.values()),
                'Or': C}
        for r,p in cons.items():
            self.ressources[r] -= p
                

    def former_metier(self, metier):
        self.population['citoyen'] -= 1
        self.population[metier] += 1

    def former_armee(self, metier, n):
        self.armee['soldat'] -= n
        self.armee[metier] += n

    def cout_unite(self):
        self.cout_unite = {'Piquier': {'Soldat': 1, 'Minerais': 7-(self.batiments['Forge'])},
                           'Cavalier': {'Soldat': 1, 'Chevaux': 7-(self.batiments['Etables'])},
                           'Archer': {'Soldat': 1, 'Lin': 7-(self.batiments['Archerie'])},
                           'Chevalier': {'Soldat': 1, 'Minerais': 7-(self.batiments['Academie_militaire']),
                                         'Chevaux': 7-(self.batiments['Academie_militaire'])},
                           'Archer_monte': {'Soldat': 1, 'Lin': 7-(self.batiments['Academie_militaire']),
                                            'Chevaux': 7-(self.batiments['Academie_militaire'])},
                           'Paladin': {'Soldat': 1, 'Minerais': 14-2*(self.batiments['Tour_de_garde'])},
                           'Catapulte': {'Bois': 72-8*self.batiments['Atelier']},
                           'Baliste': {'Bois': 270-30*self.batiments['Atelier']}}


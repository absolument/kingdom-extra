from random import randint

class Unite:
    def __init__(self, joueur, HP, loc):  #Chaque unité est sous le controle de "Joueur", possède "HP" HP et se situe en "loc"
        self.joueur = joueur
        self.loc = loc # /!\ Peut aussi être un général /!\

        self.garnison = None #True or Fals, dictera leur comportement dans la bataille
        self.barbare = None #idem
        self.HP = HP
    
    # def __str__(self):
    #     return type(self).__name__ + f' HP:{self.HP}'
    
    def attaquer(self, unite2):
        degats = randint(0,1) + self.bonus(unite2)
        unite2.HP -= degats
        
        
    def deplacer(self, newloc):
        self.loc.armee[type(self).__name__].remove(self)
        self.newloc.armee[type(self).__name__].append(self)
        self.loc = newloc
       
        
       
class Soldat(Unite):
    def __init__(self, joueur, loc):
        Unite.__init__(self, joueur, 50, loc)
        
    def bonus(self, unite2):
        return 0
    
    
    
class Piquier(Unite):
    def __init__(self,joueur, loc):
        Unite.__init__(self, joueur, 70, loc)
        
    def bonus(self, unite2):
        if type(unite2).__name__ == 'Chevalier' or type(unite2).__name__ == 'Archer_monte' :
            return 3
        else:
            return 0
        


class Cavalier(Unite):
    def __init__(self,joueur, loc):
        Unite.__init__(self, joueur, 70, loc)
        
    def bonus(self, unite2):
        if type(unite2).__name__ == 'Archer':
            return 2
        else:
            return 0

      

class Archer(Unite):
    def __init__(self,joueur, loc):
        Unite.__init__(self, joueur, 70, loc)
        
    def bonus(self, unite2):
        if type(unite2).__name__ == 'Piquier' or type(unite2).__name__ == 'Paladin' :
            return 2
        else:
            return 0
  
      

class Chevalier(Unite):
    def __init__(self, joueur, loc):
        Unite.__init__(self, joueur, 70, loc)
        
    def bonus(self, unite2):
        if type(unite2).__name__ == 'Soldat':
            return 2
        else:
            return 0      

      

class Archer_monte(Unite):
    def __init__(self, joueur, loc):
        Unite.__init__(self, joueur, 80, loc)
        
    def bonus(self, unite2):
        return 0 
      


class Paladin(Unite):
    def __init__(self, joueur, loc):
        Unite.__init__(self, joueur, 100, loc)
        
    def bonus(self, unite2):
        return 0       
    
      

class Catapulte(Unite):
    def __init__(self, joueur, loc):
        Unite.__init__(self, joueur, 60, loc)
        
    def bonus(self, unite2):
        if type(unite2).__name__ == 'Archer_monte' or self.loc.pos =='Defense' :
            return 0
        else:
            return 4
        
        
        
class Baliste(Unite):
    def __init__(self, joueur, loc):
        Unite.__init__(self, joueur, 60, loc)
        
    def bonus(self, unite2):
        if type(unite2).__name__ == 'Archer_monte' or self.loc.pos =='Attaque' :
            return 0
        else:
            return 4        
        
        
        
        
        
        

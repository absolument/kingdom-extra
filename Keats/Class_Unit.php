<?php

class Unit {
	public function __construct($player, $HP, $loc){
            $this->player = $player;
            $this->loc = $loc;
			array_push($loc->army, $this);
			$this->HP = $HP;
			$this->garrison = False;
			$this->barbarian = False;		
    }
		
	public function Attack($target){
		$damages = random_int(0,1) + $this->bonus($target);
		$target->HP = $target->HP - $damages;
	}
	
	public function Move($newloc){
		array_splice($this->loc->army, array_search($this, $$this->loc->army));
		array_push($newloc->armee, $this);
		$this->loc = $newloc;
	}
}	

class Soldier extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 50, $loc);
	}
	
	public function bonus($target){
		return 0;
	}
}

class Pikeman extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 70, $loc);
	}
	
	public function bonus($target){
		if (get_class($target) == 'Knight' or get_class($target) == 'Horseman'){
			return 3;
		} else {
		return 0;
		}
	}
}

class Horseman extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 70, $loc);
	}
	
	public function bonus($target){
		if (get_class($target) == 'Archer'){
			return 2;
		} else {
		return 0;
		}
	}
}

class Archer extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 70, $loc);
	}
	
	public function bonus($target){
		if (get_class($target) == 'Pikeman' or get_class($target) == 'Paladin'){
			return 2;
		} else {
		return 0;
		}
	}
}

class Knight extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 70, $loc);
	}
	
	public function bonus($target){
		if (get_class($target) == 'Soldat'){
			return 2;
		} else {
		return 0;
		}
	}
}

class MountedArcher extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 80, $loc);
	}
	
	public function bonus($target){
		return 0;
	}
}

class Paladin extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 100, $loc);
	}
	
	public function bonus($target){
		return 0;
	}
}

class Catapult extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 60, $loc);
	}
	
	public function bonus($target){
		if (get_class($target) == 'MountedArcher' or $this->loc->position == 'Defense'){
			return 0;
		} else {
		return 4;
		}
	}
}

class Ballista extends Unit{
	public function __construct($player, $loc){
		parent::__construct($player, 60, $loc);
	}
	
	public function bonus($target){
		if (get_class($target) == 'MountedArcher' or $this->loc->position == 'Attack'){
			return 0;
		} else {
		return 4;
		}
	}
}

?>
<?php
$RLink = array('Wheat'=>array('Farm', 'Farmer', 6), 'Wood'=> array('Hut', 'Lumberjack', 5),
			   'Gold'=>array('Market', 'Trader', 6), 'Hammer'=>array('Workshop', 'Worker', 1))
$BonusBuilding = array(1, 1.3, 1.55, 1.75, 1.9, 2);

function Alignments($grid){
	$A = array();
	for ($x=0; $x<8; $x++){
		for ($y=0; $y<8; $y++){
			$a = array([$x, $y]);
			for ($dy=1; $dy<3; $dy++){
				if ($y+$dy in_array(range(7)) and [$x, $y+$dy-1] in_array($a) and $grid($x)($y+$dy) == $grid($x)($y) != 0){    
					array_push($a, [$x, $y+$dy]);
				}
				if ($y-$dy in_array(range(7)) and [$x, $y-$dy+1] in_array($a) and $grid($x)($y-$dy) == $grid($x)($y) != 0){ 
					array_push($a, [$x, $y-$dy]);
				}
			}
			if (count($a) >= 3 and $a !in_array($A)){
				array_push($A, $a);
			}
			
			$a = array([$x, $y]);
			for ($dx=1; $dx<3; $dx++){
				if ($x+$dx in_array(range(7)) and [$x+$dx-1,$y] in_array($a) and $grid($x+$dx)($y) == $grid($x)($y) != 0){    
					array_push($a, [$x+$dx, $y]);
				}
				if ($x-$dx in_array(range(7)) and [$x-$dx+1,$y] in_array($a) and $grid($x-$dx)($y) == $grid($x)($y) != 0){ 
					array_push($a, [$x-$dx, $y]);
				}
			}
			if (count($a) >= 3 and $a !in_array($A)){
				array_push($A, $a);
		}
	}
	rsort($A);
	return $A;
}


function GenRessource(){
	if $ressources('Gold') > 0.5*array_sum($population)**2{
		$Sword = 10;     /*Valeur arbitraire, à déterminer la proba exacte*/
	} else {
		$Sword = 3;
	}
	if $ressources('Wheat') > array_sum($population)**2{
		$Citizen = 13;   /*Valeur arbitraire, à déterminer la proba exacte*/
	} else {
		$Citizen = 2;
	}
	$Wheat = $Wood = $Gold = intdiv((100-($Sword+$Citizen)), 4);
	$Turn = $Hammer = intdiv((100-($Sword+$Citizen)), 8);
	$R = array_merge(array_fill(0,$Wheat, 'Wheat'), array_fill(0,$Wood, 'Wood')
					 array_fill(0,$Gold, 'Gold'), array_fill(0,$Turn, 'Turn')
					 array_fill(0,$Hammer, 'Hammer'), array_fill(0,$Sword, 'Sword')
					 array_fill(0,$Citizen, 'Citizen'));
	return $R(random_int(0,99));
}

function GenGrid(){
	$G = array_fill(0,8,array_fill(0,8,0));
	for ($x=0; $x<8; $x++){
		for ($y=0; $y<8; $y++){
			$G($x)($y) = GenRessource();
			while (count(Alignments($G))>0){
				$G($x)($y) = GenRessource();
			}
		}
	}
}

function PossibleSwaps($grid){
	$P = array();
	for ($x=0; $x<8; $x++){
		for ($y=0; $y<8; $y++){
			for ($x2=max(0, $x-1); $x2<min($x+1, 7)+1; $x++){
				for ($y2=max(0, $y-1+abs($x2-$x)); $y2<min($y+1-abs($x2-$x),7)+1 ; $y++){
					if ([$x2, $y2] != [$x, $y]){
						$A = [[$x,$y], [$x2, $y2]];
						sort($A);
						if (!in_array($A, $P)){
							$grid($x)($y) ^= $grid($x2)($y2) ^= $grid($x)($y) ^= $grid($x2)($y2); /*Swap des cases */
							if (count(Alignments($grid)) > 0){
								array_push($P, [[$x,$y], [$x2, $y2]]);
							}
							$grid($x)($y) ^= $grid($x2)($y2) ^= $grid($x)($y) ^= $grid($x2)($y2); /*On les remet comme avant*/
						}
					}
				}
			}
		}
	}
	return $P
}
/* Dit le nombre de value dans et hors de array multidimensionnel */
function IsIn($array, $values){ 
	$res = [0,0];
	if (gettype($array(0)) != 'array'){
		foreach($values as $value){
			if in_array($array, $value){
				$res(0) += 1;
			} else {
				$res(1) += 1;
		}
		unset($value);
		return $res
	} else {
		foreach($array as $child){
			return IsIn($child, $values)
		}
	}
}

function Swap($G, $tile1, $tile2){
	[$x1, $y1] = $tile1;
	[$x2, $y2] = $tile2;
	$grid($x1)($y1) ^= $grid($x2)($y2) ^= $grid($x1)($y1) ^= $grid($x2)($y2);
	$A = Alignments($G);
	$E = array($G);
	$prod = array('Wheat'=>0, 'Gold'=>0, 'Turn'=>0, 'Hammer'=>0, 'Sword'=>0, 'Citizen'=>0)
	
	while(count($A) > 0){
		$V = array();
		for ($i=0; $i<count($A); $i++){
			if (count($V)>0){
				[$yin, $yout] = IsIn($V, array_map(function($c){return $c(1)}, $A($i)));
				[$xin, $xout] = IsIn($V
			}
		}
	}
}


?>

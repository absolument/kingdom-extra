"""
Manage landscape images in chunks.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import math
from spritemanager import Sprite, SpriteManager
from mapcell import MapCell


class MapChunkLayer:
    """Every chunk is composed of layers. In one layer, sprites come from the same image (source)
    """

    def __init__(self, sprite_source):
        """Create a ChunkLayer

        Args:
            sprite_source (string): name to identify the sprite sheet
        """
        self._sprite_source = sprite_source
        self._sprite_list = list()

    def add_sprite(self, sprite, x, y):
        """Add a sprite at the specified position

        Args:
            sprite (Sprite): the sprite to add
            x (int): x position to render the sprite
            y (int): y position to render the sprite
        """
        self._sprite_list.append([sprite.id, x, y])



class MapChunks:
    """This class manage all the chunks of a map.
    """
    def __init__(self, chunk_size, map_width, map_height):
        """Create chunk list.

        Args:
            chunk_size (int): Side length (in pixels) of the chunk (chunk is a square)
            map_width (int): The width (in pixels) of the map
            map_height (int): The height (in pixels) of the map
        """
        self._chunk_size = chunk_size
        self._nb_chunks_x = math.ceil(map_width / chunk_size) + 1
        self._nb_chunks_y = math.ceil(map_height / chunk_size) + 1

        nb_chunks = self._nb_chunks_x * self._nb_chunks_y
        self._chunks = list()
        for _ in range(nb_chunks):
            self._chunks.append(dict())


    @property
    def nb_chunks_x(self):
        """
        Returns:
            int: Number of chunks in x axis
        """
        return self._nb_chunks_x

    @property
    def nb_chunks_y(self):
        """
        Returns:
            int: Number of chunks in y axis
        """
        return self._nb_chunks_y

    @property
    def chunk_size(self):
        """
        Returns:
            int: Side lenght of a chunk
        """
        return self._chunk_size


    def add_images_from_cell(self, cell):
        """Add images from a cell to the correct chunks.

        Args:
            cell (MapCell): The MapCell from which we add images
        """
        for layer, images_layer in cell.landscape_images_layers.items():
            for image in images_layer.images:
                self._add_landscape_image(layer, images_layer.sprites_source, image.sprite, image.x, image.y)


    def export(self):
        """Export chunks data in order to render the map.

        Returns:
            dict: chunks data
        """
        nb_chunks = self._nb_chunks_x * self._nb_chunks_y

        exported_chunks = {
            "nb_chunks_x": self._nb_chunks_x,\
            "nb_chunks_y": self._nb_chunks_y,\
            "chunk_size": self._chunk_size,\
            "chunks": [[self._chunks[i][k] for k in sorted(self._chunks[i].keys())] for i in range(nb_chunks)]}

        return exported_chunks


    def _add_landscape_image(self, layer, sprite_source, sprite, x, y):
        """Add a single lanscape image

        Args:
            layer (int): the image layer id
            sprite_source (string): name to identify the sprite sheet
            sprite (Sprite): the sprite to add
            x (int): x position to render the sprite
            y (int): y position to render the sprite
        """
        min_column = max(math.floor(x / self._chunk_size), 0)
        max_column = min(math.ceil((x + sprite.width) / self._chunk_size), self._nb_chunks_x -1)
        min_line = max(math.floor(y / self._chunk_size), 0)
        max_line = min(math.ceil((y + sprite.height) / self._chunk_size), self._nb_chunks_y -1)

        for l in range(min_line, max_line + 1):
            for c in range(min_column, max_column + 1):
                id_chunk = l * self._nb_chunks_y + c
                if not layer in self._chunks[id_chunk]:
                    self._chunks[id_chunk][layer] = dict()
                    self._chunks[id_chunk][layer]["source"] = sprite_source
                    self._chunks[id_chunk][layer]["sprites"] = list()
                self._chunks[id_chunk][layer]["sprites"].append([sprite.id, x, y])

"""
This module manages landscape images in cells. A cell is a part of the part delimited by paths.
Each cell is associated to a landform, and we add images related to this landform.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import random
import math
from sprite import LandscapeImage


class LanscapeImagesLayer:
    """Images are drawns with layers. For example a layer for the ground, another for trees.
    This class implements a layer. A layer uses sprites from the same source.
    """
    def __init__(self, sprites_source):
        """Create an empty LanscapeImagesLayer.

        Args:
            sprites_source (string): key to identity the sprite sheet to use
        """
        self._sprites_source = sprites_source
        self._images = list()

    @property
    def sprites_source(self):
        """Get the key to the sprite sheet

        Returns:
            string: key to identity the sprite sheet
        """
        return self._sprites_source

    @property
    def images(self):
        """Get the images in the layer

        Returns:
            list of LandscapeImage: the images in the layer
        """
        return self._images       

    def add_image(self, sprite, x, y):
        """Add an image to the layer

        Args:
            sprite (Sprite): the sprite to use
            x (int): x position of the sprite
            y (int): y position of the sprite
        """
        self._images.append(LandscapeImage(sprite, x, y))



class MapCell:
    """A cell in the map: the area delimited by paths.
    """
    def __init__(self, cell_cities, map_width, map_height):
        """Create an empty cell from the cities at the cell vertices.

        Args:
            cell_cities (list of City): cities at the cell vertices
            map_width ([type]): width of the map
            map_height ([type]): height of the map
        """
        self._cities = cell_cities
        self._map_width = map_width
        self._map_height = map_height

        x_list = [city.x for city in self._cities]
        y_list = [city.y for city in self._cities]

        self._min_x = min(x_list)
        self._max_x = max(x_list)
        self._min_y = min(y_list)
        self._max_y = max(y_list)   

        self._sampling = dict()
        self._landscape_images_layers = dict()


    @property
    def landscape_images_layers(self):
        """Get lanscape images layers.

        Returns:
            list of LanscapeImagesLayer: lanscape images layer
        """
        return self._landscape_images_layers


    def fill_with_sprites(self, landform_data, sprite_manager, map_boundary = False):
        """Fill the cell with sprites, depending on landform

        Args:
            landform_data (dict): parameters for landscape generation
            sprite_manager (SpriteManager): we access sprites through this manager
            map_boundary (bool, optional): Is the cell at the map external boundary. Defaults to False.
        """
        for type_layer, layer_list in landform_data.items():
            if type_layer == "floating_sprites_layers":
                for layer in layer_list:
                    self._fill_floating_sprites_layer(layer, sprite_manager, map_boundary)
            elif type_layer == "marching_square_layers":
                for layers in layer_list:
                    self._fill_marching_square_tiles(layers, sprite_manager)
            elif type_layer == "horizontal_tiles_layers":
                for layer in layer_list:
                    self._fill_horizontal_tiles(layer, sprite_manager, map_boundary)                


    def _fill_floating_sprites_layer(self, layer_dict, sprite_manager, map_boundary = False):
        """Fill the layer with independant 'floating' sprites.

        Args:
            layer_dict (dict): parameters for layer sprites generation
            sprite_manager (SpriteManager): we access sprites through this manager
            map_boundary (bool, optional): Is the cell at the map external boundary. Defaults to False.
        """
        if map_boundary:
            min_y = 0
            max_y = self._map_height
        else:
            y_list = [city.y for city in self._cities]
            min_y = min(y_list)
            max_y = max(y_list)

        can_overflow = layer_dict.get("can_overflow", 0)

        if layer_dict["layer"] == 0:
            default_delta_city = 1
            default_delta_path = 1
        else:
            default_delta_city = 18
            default_delta_path = 5

        delta_city = layer_dict.get("delta_city", default_delta_city)
        delta_path = layer_dict.get("delta_path", default_delta_path)

        y_ground = layer_dict.get("y_ground", 0)

        layer = layer_dict["layer"]
        if not layer in self._landscape_images_layers:
            self._landscape_images_layers[layer] = LanscapeImagesLayer(layer_dict["source"])


        for y in range(min_y, max_y, layer_dict["dy"]):
            intervals = self._x_intervals(y, y + layer_dict["y_sampling"], delta_path, delta_city, map_boundary)

            for i in range(0,len(intervals),2):
                x = intervals[i]
                sprites_in_row = list()
                while x < intervals[i+1]:
                    sprite_name = random.choice(layer_dict["sprites"])
                    sprite = sprite_manager.get_sprite(layer_dict["source"], sprite_name)

                    if (x + sprite.width < intervals[i+1] + can_overflow) or (not len(sprites_in_row) and can_overflow > 0):
                        sprites_in_row.append([sprite, x])
                    delta_x = layer_dict["dx"] + random.randint(-layer_dict["rand_max_x"], layer_dict["rand_max_x"])
                    if x + delta_x > intervals[i] and abs(delta_x) > 1:
                        x += delta_x
                    else:
                        x += layer_dict["dx"]

                if len(sprites_in_row):
                    last_sprite = sprites_in_row[-1][0]
                    last_sprite_x = sprites_in_row[-1][1]
                    end_row = last_sprite_x + last_sprite.width
                    delta_x = intervals[i+1] - end_row
                for sprite_and_pos in sprites_in_row:
                    sprite = sprite_and_pos[0]
                    if delta_x > 0:
                        x = sprite_and_pos[1] + random.randint(0, math.floor(delta_x))
                    elif delta_x < 0:
                         x = sprite_and_pos[1] - random.randint(0, math.floor(-delta_x))
                    else:
                        x = sprite_and_pos[1]
                    
                    self._landscape_images_layers[layer].add_image(sprite, x, y + y_ground - sprite.height + sprite.y_offset)



    def _fill_marching_square_tiles(self, layers_dict, sprite_manager):
        """Fill the layer using marching square tiles.

        Args:
            layer_dict (dict): parameters for layer sprites generation
            sprite_manager (SpriteManager): we access sprites through this manager
        """
        tile_size = sprite_manager.get_tile_size(layers_dict["marching_square_tiles"]["source"])

        delta_city = layers_dict["marching_square_tiles"].get("delta_city", 50)
        delta_path = layers_dict["marching_square_tiles"].get("delta_path", 30)


        min_x = self._min_x - (self._min_x % tile_size)
        min_y = self._min_y - (self._min_y % tile_size)
        max_x = self._max_x + tile_size - (self._max_x % tile_size)
        max_y = self._max_y + tile_size - (self._max_y % tile_size)       
        
        self._sample(min_x, max_x, tile_size, min_y, max_y, tile_size, delta_path, delta_city)

        ms_layer = layers_dict["marching_square_tiles"]["layer"]
        if not ms_layer in self._landscape_images_layers:
            self._landscape_images_layers[ms_layer] = LanscapeImagesLayer(layers_dict["marching_square_tiles"]["source"])

        has_on_tile_layer = ("on_tiles_sprites" in layers_dict)
        if has_on_tile_layer:
            on_tile_layer = layers_dict["on_tiles_sprites"]["layer"]
            if not on_tile_layer in self._landscape_images_layers:
                self._landscape_images_layers[on_tile_layer] = LanscapeImagesLayer(layers_dict["on_tiles_sprites"]["source"])


        for y in range(min_y, max_y - tile_size, tile_size):
            for x in range(min_x, max_x - tile_size, tile_size):
                square_value = 0
                if self._sampling[(x, y)]:
                    square_value += 8
                if self._sampling[(x + tile_size, y)]:
                    square_value += 4
                if self._sampling[(x + tile_size, y + tile_size)]:
                    square_value += 2
                if self._sampling[(x, y + tile_size)]:
                    square_value += 1

                if square_value:
                    square_key = 'cells_' + str(square_value)
                    sprite_name = random.choice(layers_dict["marching_square_tiles"][square_key])
                    sprite = sprite_manager.get_sprite(layers_dict["marching_square_tiles"]["source"],sprite_name)
                    self._landscape_images_layers[ms_layer].add_image(sprite, x, y)

                    if has_on_tile_layer:
                        cell_key = 'cell_' + str(square_value)
                        rand_max_x = layers_dict["on_tiles_sprites"].get("rand_max_x", 0)
                        rand_max_y = layers_dict["on_tiles_sprites"].get("rand_max_y", 0)
                        if cell_key in layers_dict["on_tiles_sprites"]:
                            centers = layers_dict["on_tiles_sprites"][cell_key]
                            for center in centers:
                                dx = center[0] + random.randint(-rand_max_x, rand_max_x)
                                dy = center[1] + random.randint(-rand_max_y, rand_max_y)
                                sprite_name = random.choice(layers_dict["on_tiles_sprites"]["sprites"])
                                sprite = sprite_manager.get_sprite(layers_dict["on_tiles_sprites"]["source"],sprite_name)

                                self._landscape_images_layers[on_tile_layer].add_image(sprite, x + dx, y + dy)


    def _fill_horizontal_tiles(self, layer_dict, sprite_manager, map_boundary = False):
        """Fill the layer using horizontal tiles.

        Args:
            layer_dict (dict): parameters for layer sprites generation
            sprite_manager (SpriteManager): we access sprites through this manager
            map_boundary (bool, optional): Is the cell at the map external boundary. Defaults to False.
        """
        if map_boundary:
            min_y = 0
            max_y = self._map_height
        else:
            y_list = [city.y for city in self._cities]
            min_y = min(y_list)
            max_y = max(y_list)

        can_overflow = layer_dict.get("can_overflow", 0)

        if layer_dict["layer"] == 0:
            default_delta_city = 1
            default_delta_path = 1
        else:
            default_delta_city = 18
            default_delta_path = 5

        delta_city = layer_dict.get("delta_city", default_delta_city)
        delta_path = layer_dict.get("delta_path", default_delta_path)

        dy = layer_dict.get("dy", 0)

        layer = layer_dict["layer"]
        if not layer in self._landscape_images_layers:
            self._landscape_images_layers[layer] = LanscapeImagesLayer(layer_dict["source"])

        tile_width = sprite_manager.get_htile_width(layer_dict["source"])
        tile_height = sprite_manager.get_htile_height(layer_dict["source"])
        nb_patterns = sprite_manager.get_htile_nb_patterns(layer_dict["source"])

        for y in range(min_y, max_y - tile_height, dy):
            intervals = self._x_intervals(y, y + tile_height, delta_path, delta_city, map_boundary)

            for i in range(0,len(intervals),2):
                x = intervals[i]
                sprites_in_row = list()
                left_pattern = 0
                while x < intervals[i+1] - 2 * tile_width + can_overflow:
                    right_pattern = random.choice([k for k in range(0, nb_patterns+1) if k != left_pattern])
                    sprite_key = "tile_" + str(left_pattern) + "_" + str(right_pattern)
                    sprite = sprite_manager.get_sprite(layer_dict["source"], sprite_key)
                    sprites_in_row.append([sprite, x])
                    x += tile_width
                    left_pattern = right_pattern

                if len(sprites_in_row):
                    if left_pattern != 0:
                        sprite_key = "tile_" + str(left_pattern) + "_" + str(0)
                        sprite = sprite_manager.get_sprite(layer_dict["source"], sprite_key)
                        sprites_in_row.append([sprite, x])
                        x += tile_width

                    delta_x = intervals[i+1] - x
                
                    if intervals[i+1] - x > 0:
                        delta_x = random.randint(0, math.floor(intervals[i+1] + can_overflow - x))
                    elif intervals[i+1] - x < 0:
                        delta_x = - random.randint(0, math.floor(x - intervals[i+1]))
                    else:
                        delta_x = 0

                for sprite_and_pos in sprites_in_row:
                    sprite = sprite_and_pos[0]
                    self._landscape_images_layers[layer].add_image(sprite, sprite_and_pos[1] + delta_x, y)                
    

    def _sample(self, min_x, max_x, dx, min_y, max_y, dy, delta_path, delta_city):
        """Compute if points in an aligned axis rectangle are in the cell. Write results in self._sampling.

        Args:
            min_x (int): minimum x coordinate of the rectangle
            max_x (int): maximum x coordinate of the rectangle
            dx (int): x sampling step
            min_y (int): minimum y coordinate of the rectangle
            max_y (int): maximum y coordinate of the rectangle
            dy (int): y sampling step
            delta_path (int): margin in pixels to add along paths
            delta_city (int): margin in pixels to add around cities
        """

        for y in range(min_y + dy, max_y, dy):
            intervals = self._x_intervals(y, y + dy, 10, 10, False)
            for x in range(min_x, max_x, dx):
                inside_cell = False
                for k in range(0,len(intervals),2):
                    if intervals[k] <= x and x <= intervals[k+1]:
                        inside_cell = self._is_position_far_enought(delta_path, delta_city, x, y)
                self._sampling[(x,y)] = inside_cell

        for x in range(min_x, max_x, dx):
            self._sampling[(x, min_y)] = False
            self._sampling[(x, max_y + dy)] = False


    def _is_position_far_enought(self, delta_path, delta_city, x, y):
        """Check if (x,y) point distance to every path is at least delta_path,
        and distance to every city is at least delta_city

        Args:
            delta_path (int): minimum distance to the paths
            delta_city (int): minimum distance to the cities
            x (int): x coordinate of the point to check
            y (int): y coordinate of the point to check

        Returns:
            bool: is the point far enought paths and cities
        """
        min_dist_to_path = self._map_width + self._map_height
        min_dist_to_city = self._map_width + self._map_height

        c1 = self._cities[-1]

        for c2 in self._cities:
            dist2_to_city = (x - c2.x) * (x - c2.x) + (y - c2.y) * (y - c2.y)
            min_dist_to_city = min(math.sqrt(dist2_to_city), min_dist_to_city)

            #compute squared distance between (x,y) point and c1c2 segment
            if (x-c1.x) * (c2.x - c1.x) + (y - c1.y) * (c2.y - c1.y) < 0:
                dist2 = (x - c1.x) * (x - c1.x) + (y - c1.y) * (y - c1.y)
            elif (x - c2.x) * (c1.x - c2.x) + (y-c2.y) * (c1.y - c2.y) < 0:
                dist2 = dist2_to_city
            else:
                a = (c2.y - c1.y) * x - (c2.x - c1.x) * y - c1.x * c2.y + c1.y * c2.x
                dist2 = a * a / ((c2.y - c1.y) * (c2.y - c1.y) + (c2.x - c1.x) * (c2.x - c1.x))

            min_dist_to_path = min(math.sqrt(dist2), min_dist_to_path)

            c1 = c2

        if min_dist_to_path < delta_path:
            return False
        if min_dist_to_city < delta_city:
            return False
        return True


    def _x_intervals(self, y1, y2, delta_path, delta_city, map_boundary):
        """Search [x1,x2] intervals such as the aligned axis rectangle from (x1,y1) to (x2,y2)
        is totally included in the cell. Returns a list of x values.

        Args:
            y1 (int): y coordinate of the rectangles
            y2 (int): other y coordinate of the rectangles
            delta_path (int): minimum distance to the paths
            delta_city (int): minimum distance to the cities
            map_boundary (bool): if the cell at the boundary of the map

        Returns:
            list of int: x coordinates of the rectangles
        """
        intervals = list()
        list1 = self._x_intersect_horizontal(y1, delta_path, map_boundary)
        list2 = self._x_intersect_horizontal(y2, delta_path, map_boundary)
        i1 = 0
        i2 = 0

        while i1 < len(list1) and i2 < len(list2):
            if list1[i1] > list2[i2+1]:
                i2 += 2
            if i2 >= len(list2)-1:
                break
            if list2[i2] > list1[i1+1]:
                i1 += 2
            if i1 >= len(list1)-1:
                break
            if list1[i1] <= list2[i2+1] and list2[i2] <= list1[i1+1]:
                intervals.append(max(list1[i1],list2[i2]))
                if list1[i1+1] < list2[i2+1]:
                    intervals.append(list1[i1+1])
                    i1 += 2
                else:
                    intervals.append(list2[i2+1])
                    i2 += 2

        adapted_interval = list()
        for i in range(0,len(intervals),2):
            adapted_interval += self._adapt_x_interval(intervals[i], intervals[i+1], y1, y2, delta_path, delta_city)

        return adapted_interval


    def _x_intersect_horizontal(self, y, delta_path, map_boundary):
        """Search where the horizontal line (y coordinate) cross the paths of the cell with a delta_path margin
        along paths

        Args:
            y (int): y coordinate of the horizontal line
            delta_path (int): margin along the paths
            map_boundary (bool): if the cell at the boundary of the map

        Returns:
            list of int: list of x coordinates where the line cross the paths with delta_path margin
        """
        float_intersect = list()

        c0 = self._cities[-2]
        c1 = self._cities[-1]
        need_to_remove_last = False

        for c2 in self._cities:
            if c1.y != c2.y:
                if y != c1.y or (c2.y - c1.y) * (c1.y - c0.y) < 0 or (c0.y == c1.y and (c1.x - c0.x) * (c2.y - c1.y) >= 0):
                    x = c1.x + (y - c1.y) * (c2.x - c1.x) / (c2.y - c1.y)
                    if (c1.x != c2.x and ((c2.x <= x <= c1.x) or (c1.x <= x <= c2.x))) or \
                        (c1.x == c2.x and ((c2.y <= y <= c1.y) or (c1.y <= y <= c2.y))) :
                        float_intersect.append(x)
            elif y == c2.y:
                if (c2.x - c1.x) * (c1.y - c0.y) >= 0 and c1.y != c0.y:
                    if float_intersect:
                        float_intersect.pop()
                    else:
                        need_to_remove_last = True

            c0 = c1
            c1 = c2

        if need_to_remove_last and float_intersect:
            float_intersect.pop()

        float_intersect.sort()

        if len(float_intersect)%2:
            print("Error in images position computation: intersection list should be even.")
            return []

        if map_boundary:
            if not float_intersect:
                return [0,self._map_width]

            int_intersect = [0, math.floor(float_intersect[0]) - delta_path]
            for i in range(1, len(float_intersect)-1, 2):
                x1 = math.floor(float_intersect[i]) + delta_path
                x2 = math.floor(float_intersect[i+1]) - delta_path
                if x1 < x2:
                    int_intersect.append(x1)
                    int_intersect.append(x2)
            int_intersect.append(math.floor(float_intersect[-1]) + delta_path)
            int_intersect.append(self._map_width)

        else:
            int_intersect = []
            for i in range(0, len(float_intersect), 2):
                x1 = math.floor(float_intersect[i]) + delta_path
                x2 = math.floor(float_intersect[i+1]) - delta_path
                if x1 < x2:
                    int_intersect.append(x1)
                    int_intersect.append(x2)           

        return int_intersect


    def _adapt_x_interval(self, x1, x2, y1, y2, delta_path, delta_city):
        """Find the largest aligned axis rectangle(s) included in aligned axis rectangle from (x1,y1) to (x2,y2)
        which are included in the cell

        Args:
            x1 (int): x coordinate of the rectangle
            x2 (int): other x coordinate of the rectangle
            y1 (int): y coordinate of the rectangle
            y2 (int): other y coordinate of the rectangle
            delta_path (int): minimum distance to the paths
            delta_city (int): minimum distance to the cities

        Returns:
            list of int: x coordinates of the rectangles
        """
        
        c1 = self._cities[-1]

        for c2 in self._cities:
            if self._does_path_intersect_rectangle(c1, c2, x1, x2, y1, y2):
                testx1 = max(c1.x, c2.x) + delta_path
                testx2 = min(c1.x, c2.x) - delta_path

                if testx1 < x2 and not self._does_path_intersect_rectangle(c1, c2, testx1, x2, y1, y2):
                    x1 = testx1
                elif testx2 > x1 and not self._does_path_intersect_rectangle(c1, c2, x1, testx2, y1, y2):
                    x2 = testx2
                elif testx1 < testx2 and not self._does_path_intersect_rectangle(c1, c2, testx1, testx2, y1, y2):
                    x1 = testx1
                    x2 = testx2
                else:
                    return []
            c1 = c2

        interval = [x1,x2]

        for c in self._cities:
            city_x = c.x
            city_y = c.y

            if y1 - delta_city < city_y and city_y < y2 + delta_city:
                i = 0
                while(i<len(interval)):
                    inc = 0
                    if interval[i] - delta_city <=  city_x  and city_x  <= interval[i+1] + delta_city:
                        if city_x + delta_city < interval[i+1]:
                            interval.insert(i+1, city_x + delta_city)
                            inc += 1
                        else:
                            del interval[i+1]
                            inc -= 1
                        if interval[i] < city_x - delta_city:
                            interval.insert(i+1, city_x - delta_city)
                            inc += 1
                        else:
                            del interval[i]
                            inc -= 1
                    i += 2 + inc

        return interval


    def _does_path_intersect_rectangle(self, c1, c2, x1, x2, y1, y2):
        """Check if the path from c1 to c2 intersect an aligned axis rectangle.

        Args:
            c1 (City): first city of the path
            c2 (City): second city of the path
            x1 (int): x coordinate of the rectangle
            x2 (int): other x coordinate of the rectangle
            y1 (int): y coordinate of the rectangle
            y2 (int): other y coordinate of the rectangle

        Returns:
            bool: does the path intersect the rectangle
        """
        if  ((c1.x >= x1) or (c2.x >= x1)) and \
            ((c1.x <= x2) or (c2.x <= x2)) and \
            ((c1.y >= y1) or (c2.y >= y1)) and \
            ((c1.y <= y2) or (c2.y <= y2)):

            f11 =  (c2.y - c1.y) * x1 + (c1.x - c2.x) * y1 + (c2.x * c1.y - c1.x * c2.y)
            f12 =  (c2.y - c1.y) * x1 + (c1.x - c2.x) * y2 + (c2.x * c1.y - c1.x * c2.y)
            f21 =  (c2.y - c1.y) * x2 + (c1.x - c2.x) * y1 + (c2.x * c1.y - c1.x * c2.y)
            f22 =  (c2.y - c1.y) * x2 + (c1.x - c2.x) * y2 + (c2.x * c1.y - c1.x * c2.y)

            if  (f11 < 0 or f12 < 0 or f21 < 0 or f22 < 0) and \
                (f11 > 0 or f12 > 0 or f21 > 0 or f22 > 0):
                return True

        return False



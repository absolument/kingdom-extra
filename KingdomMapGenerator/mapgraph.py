"""
Manage the graph of a map: the cities and the links between them.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import math
import random
import re

from city import City
from namegenerator import NameGenerator
from voronoi import Voronoi


class MapGraph:
    """The graph of the map: cities and links between them.
    """
    def __init__(self):
        """Create an empty graph
        """
        self._cities = list()

    @property
    def cities(self):
        """
        Returns:
            list of City: the cities of the graph
        """
        return self._cities

    @property
    def voronoi_vertices(self):
        """
        Returns:
            list of list of int: coordinates of the vertices of voronoi cells
        """
        return self._voronoi_vertices

    @property
    def voronoi_regions(self):
        """
        Returns:
            dict: keys are cities ids.
            Values contains the list of vertices, and the list of adjacent regions (identified by their city id)        """
        return self._voronoi_regions


    def load(self, cities_data_list, language, map_width, map_height, margin):
        """Loads a graph from a list of cities

        Args:
            cities_data_list (list of City): the cities of the graph
            language (string): map language (for name generation)
            map_width (int): width of the map in pixels
            map_height (int): height of the map in pixels
            margin (int): number of pixels on the side of the map without any city
        """
        name_gen = NameGenerator(language)
        self._cities = [City(city_data["x"], city_data["y"], name_gen, city_data["cap"]) for city_data in cities_data_list]

        for (city_data, city) in zip(cities_data_list, self._cities):
            city.name = city_data["name"]
            for adj in city_data["adj"]:
                city.add_link_to_city(self._cities[adj])

        self._sort_links_ccw()
        self._compute_voronoi(map_width, map_height, margin/2)


    def export_cities(self):
        """Export cities data

        Returns:
            list of dict: each element of the list contains the data for a City
        """
        city_to_id = {city: city_id for city_id,city in enumerate(self._cities)}
        cities_list = list()

        for city in self._cities:
            city_item = { \
                'x': city.x,\
                'y': city.y,\
                'cap': city.is_capital,\
                'name': city.name,\
                'adj': [city_to_id[adj_city] for adj_city in city.adjacent_cities]
            }
            cities_list.append(city_item)
        
        return cities_list


    def generate(self, map_width, map_height, gen_params):
        """Generate the graph

        Args:
            map_width (int): width of the map in pixels
            map_height (int): height of the map in pixels
            gen_params (dict): generation parameters
        """

        one_hour_dist = gen_params["one_hour_dist"]
        margin = gen_params["margin"]

        initial_dist = one_hour_dist * gen_params["initial_distance"]
        nb_columns = math.floor((map_width - 2 * margin) / initial_dist)
        nb_lines =  math.floor((map_height - 2 * margin) / (math.sqrt(3) / 2 * initial_dist))
        x_offset = (map_width - initial_dist * nb_columns ) // 2
        y_offset = math.floor((map_height - math.sqrt(3) / 2* initial_dist * nb_lines) / 2)

        self._prepare_cities_hexa(\
            nb_lines, nb_columns, x_offset, y_offset, \
            one_hour_dist * gen_params["initial_distance"], \
            NameGenerator(gen_params["language"]))
        self._place_capitals_hexa(nb_lines, nb_columns)

        self._random_move_cities(one_hour_dist * gen_params["random_radius"])
        self._merge_too_close_cities(one_hour_dist * gen_params["minimum_distance"])
        self._remove_links_between_close_capitals()
        self._delete_too_long_links(one_hour_dist * gen_params["maximum_distance"])
        self._delete_intersecting_links()
        self._sort_links_ccw()
        self._remove_too_sharp_angles(gen_params["minimum_angle"])
        self._limit_adjacency(4)
        self._delete_disconnected_cities()
        self._adapt_capitals()
        self._change_invalid_capitals_to_cities()

        self._compute_voronoi(map_width, map_height, margin/2)


    def find_cells(self):
        """Find the cells of the graph : a cell is an area delimited by paths.

        Returns:
            list of Cell: the cells of the graph
        """
        link_visit = dict()
        cells_with_area = list()
        for city in self._cities:
            for adj in city.adjacent_cities:
                link_visit[(city, adj)] = False

 
        remaining_links = [link for link, value in link_visit.items() if not value]
        while len(remaining_links):
            c1, c2 = remaining_links[0]
            cell_cities = [c1]

            while c2 != cell_cities[0]:
                cell_cities.append(c2)
                link_visit[(c1, c2)] = True
                search_pos = c2.adjacent_cities.index(c1)
                c1 = c2
                c2 = c2.adjacent_cities[search_pos - 1]

            cells_with_area.append({'cities':cell_cities, 'area': self._compute_cell_area(cell_cities)})
            link_visit[(c1, c2)] = True
            remaining_links = [link for link, visited in link_visit.items() if not visited]

        sorted_cells = sorted(cells_with_area, key=lambda k: k['area'], reverse=True)
        return [cell['cities'] for cell in sorted_cells]



    def get_paths_lines_to_draw(self):
        """Build a list with the lines to draw to represents the paths. Each line is defined with two points.

        Returns:
            list of list of int: coordinates of points that defined the lines
        """
        paths_lines = list()

        for i, city in enumerate(self._cities):
            x1 = city.x
            y1 = city.y

            for adj in city.adjacent_cities:
                if not adj in self._cities[:i]:
                    x2 = adj.x
                    y2 = adj.y
                    dist = math.sqrt(city.dist2_to_city(adj))

                    if city.is_capital:
                        ratio_1 = 20 / dist
                    else:
                        ratio_1 = 15 / dist

                    if adj.is_capital:
                        ratio_2 = 20 / dist
                    else:
                        ratio_2 = 15 / dist
                    paths_lines.append([\
                        math.floor(x1 + (x2 - x1) * ratio_1), math.floor(y1 + (y2 - y1) * ratio_1),\
                        math.floor(x2 - (x2 - x1) * ratio_2), math.floor(y2 - (y2 - y1) * ratio_2)])

        return paths_lines


    def _prepare_cities_hexa(self, nb_lines, nb_columns, x_offset, y_offset, initial_dist, name_gen):
        """Create cities in the map with an hexagonal pattern

        Args:
            nb_lines (int): number of cities in y direction
            nb_columns (int): number of cities in x direction
            x_offset (int): margin in x direction
            y_offset (int): margin in y direction
            initial_dist (int): distance between cities
            name_gen (NameGenerator): name generator to use
        """
        for i in range(nb_lines):
            for j in range(nb_columns):
                if i % 2 == 0:
                    x = math.floor(x_offset + initial_dist * (j + 0.75))
                else:
                    x = math.floor(x_offset + initial_dist * (j + 0.25))
                y = y_offset + math.floor(initial_dist * (0.5 + math.sqrt(3) / 2 * i))
                self._cities.append(City(x, y, name_gen))

        for i, city_1 in enumerate(self._cities):
            for city_2 in self._cities[:i]:
                if city_1.dist2_to_city(city_2) < initial_dist * initial_dist * 1.1:
                    city_1.add_link_to_city(city_2)



    def _place_capitals_hexa(self, nb_lines, nb_columns):
        """Change cities to capitals

        Args:
            nb_lines (int): number of cities in y direction
            nb_columns (int): number of cities in x direction
        """

        for i in range(nb_lines):
            for j in range((1+7*(i%2)+3*i)%13, nb_columns, 13):
                self._cities[i * nb_columns + j].is_capital = True


        city_list = list()
        for i in range(-1, nb_lines):
            for j in range((3+7*(i%2)+3*i)%13, nb_columns, 13):
                sub_list = list()
                city_id = i * nb_columns + j
                if i>=0:
                    sub_list.append(city_id)
                else:
                    sub_list.append(-1)
                if i<nb_lines-1:
                    if i%2 or j>0:
                        sub_list.append(city_id + nb_columns -i%2)
                    else:
                        sub_list.append(-1)
                    if i%2==1 or j<nb_columns-1:
                        sub_list.append(city_id + nb_columns + (i+1)%2)
                    else:
                        sub_list.append(-1)
                city_list.append(sub_list)

        for sub_list in city_list:
            capital_id = random.choice(sub_list)
            if(capital_id >= 0):
                self._cities[capital_id].is_capital = True

        city_list = list()
        for i in range(0, nb_lines+1):
            for j in range((-1+7*(i%2)+3*i)%13, nb_columns, 13):
                sub_list = list()
                city_id = i * nb_columns + j
                if i<nb_lines:
                    sub_list.append(city_id)
                else:
                    sub_list.append(-1)
                if i>0:
                    if i%2==0 or j>0:
                        sub_list.append(city_id - nb_columns - i%2)
                    else:
                        sub_list.append(-1)
                    if i%2 or j<nb_columns-1:
                        sub_list.append(city_id - nb_columns + (i+1)%2)
                    else:
                        sub_list.append(-1)    
                city_list.append(sub_list)

        for sub_list in city_list:
            capital_id = random.choice(sub_list)
            if(capital_id >= 0):
                if not self._cities[capital_id].is_adjacent_with_capital():
                    self._cities[capital_id].is_capital = True


    def _remove_links_between_close_capitals(self):
        """ Remove links in the graph so that the shortest path between 2 capitals have at least 2 other cities.
        """
        capital_list = [city for city in self._cities if city.is_capital]
        paths_between_close_capitals = list()

        for city in capital_list:
            for adj1 in city.adjacent_cities:
                for adj2 in adj1.adjacent_cities:
                    if adj2 != city and adj2.is_capital and not [adj2,adj1,city] in paths_between_close_capitals:
                        paths_between_close_capitals.append([city,adj1,adj2])

        for path in paths_between_close_capitals:
            if len(path[0].adjacent_cities) > len(path[2].adjacent_cities):
                path[0].remove_link_to_city(path[1])
            else:
                path[2].remove_link_to_city(path[1])


    def _random_move_cities(self, radius):
        """Move cities randomly from their original position

        Args:
            radius (float): the maximum displacment of each city
        """
        for city in self._cities:
            r = random.uniform(0, radius)
            angle = random.uniform(0, 2.*math.pi)
            city.translate(math.floor(r * math.cos(angle)), math.floor(r * math.sin(angle)))  
        


    def _merge_too_close_cities(self, min_dist):
        """Merge cities which are at a distance smaller than min_dist

        Args:
            min_dist (float)
        """
        merge = True

        while merge:
            to_merge_list = list()
            to_delete_list = list()
            for i, city_1 in enumerate(self._cities):
                for city_2 in self._cities[:i]:
                    dist2 = city_1.dist2_to_city(city_2)
                    if dist2 < min_dist * min_dist:
                        if city_1.is_adjacent_to(city_2):
                            #we will merge city_1 with city_2
                            to_merge_list.append([city_1, city_2])
                        else:
                            if dist2 == 0:
                                to_delete_list.append(city_2)
                            else:
                                #we do not merge non adjacent cities, but translate them away
                                ratio = min_dist * min_dist / dist2
                                dx = math.ceil((city_2.x - city_1.x) * ratio / 2)
                                dy = math.ceil((city_2.y - city_1.y) * ratio / 2)
                                city_1.translate(dx, dy)
                                city_2.translate(-dx, -dy)

            merged_couples = 0
            for close_cities in to_merge_list:
                if (close_cities[0] in self._cities) and (close_cities[1] in self._cities):
                    self._merge_cities(close_cities[0], close_cities[1])
                    merged_couples += 1
            merge = (merged_couples > 0)
        
        for city in to_delete_list:
            self._cities.remove(city)


    def _delete_too_long_links(self, max_dist):
        """Delete links which have a lenght greater than max_dist

        Args:
            max_dist (float)
        """
        too_long_links = list()
        for city in self._cities:
            for adj_city in city.adjacent_cities:
                if not ([adj_city, city] in too_long_links) and city.dist2_to_city(adj_city) > max_dist * max_dist:
                    too_long_links.append([city, adj_city])

        for long_link in too_long_links:
            long_link[0].remove_link_to_city(long_link[1])


    def _delete_intersecting_links(self):
        """When two links intersects, remove the longuest.
        """
        paths_list = list()
        for city in self._cities:
            for adj_city in city.adjacent_cities:
                if not [adj_city, city] in paths_list:
                    paths_list.append([city, adj_city])

        intesecting_list = list()
        for i, link_1 in enumerate(paths_list):
            for link_2 in paths_list[:i]:
                city1_link1 = link_1[0]
                city2_link1 = link_1[1]
                city3_link2 = link_2[0]
                city4_link2 = link_2[1]                

                if (city2_link1 != city3_link2) and (city1_link1 != city4_link2) and \
                    (city1_link1 != city3_link2) and (city2_link1 != city4_link2) and \
                    self._links_intersects(city1_link1, city2_link1, city3_link2, city4_link2):
                    intesecting_list.append([link_1,link_2])

        for intersection in intesecting_list:
            self._remove_longest_link(intersection)
 

    def _sort_links_ccw(self):
        """For each City, sorts adjacent (linked) cities to make them in counter clockwise order
        """
        for city in self._cities:
             city.sort_links_ccw()


    def _remove_too_sharp_angles(self, min_angle):
        """When the angle between two links is sharper than min_angle, remove the longuest link

        Args:
            min_angle (float)
        """
        for city in self._cities:
            nb_adj = len(city.adjacent_cities)
            delta = 1

            while nb_adj > 1 and delta:
                sharp_angle_couples = list()
                c1 = city.adjacent_cities[-1]
                x1 = c1.x - city.x
                y1 = c1.y - city.y
                for c2 in city.adjacent_cities:
                    x2 = c2.x - city.x
                    y2 = c2.y - city.y
                    angle = math.atan2(y1,x1) - math.atan2(y2,x2)
                    if angle > math.pi:
                        angle -= 2 * math.pi
                    if angle <= -math.pi:
                        angle += 2 *math.pi
                    if abs(angle) < min_angle:
                        sharp_angle_couples.append([[city,c1],[city,c2]])
                    c1 = c2
                    x1 = x2
                    y1 = y2
                for couple in sharp_angle_couples:
                    self._remove_longest_link(couple)

                nb_adj_old = nb_adj
                nb_adj = len(city.adjacent_cities)
                delta = nb_adj - nb_adj_old


    def _limit_adjacency(self, max_adjacency):
        """Remove links so that each city has not more than max_adjacency links

        Args:
            max_adjacency (int)
        """
        for city in self._cities:
            while len(city.adjacent_cities) > max_adjacency:
                city.remove_link_to_most_linked_city()


    def _delete_disconnected_cities(self):
        """Partition the Graph with sets of (directly or indirectly) connected cities. Keep only the largest set, 
        remove the other cities. So there is a path between every couple of remaining cities.
        """
        unvisited_cities = [city for city in self._cities]
        partitions = list()

        while len(unvisited_cities):
            current_partition = list()
            next_step_cities = [unvisited_cities.pop()]

            while len(next_step_cities):
                current_partition += next_step_cities
                current_step_cities = next_step_cities
                next_step_cities = list()
                for city in current_step_cities:
                    for adj in city.adjacent_cities:
                        if adj in unvisited_cities:
                            next_step_cities.append(adj)
                            unvisited_cities.remove(adj)
            partitions.append(current_partition)

        len_main_partition = max([len(partition) for partition in partitions])
        main_found = False
        for partition in partitions:
            if len(partition) < len_main_partition or (main_found and len(partition) == len_main_partition):
                for city in partition:
                    self._remove_city(city)
            

    def _remove_longest_link(self, link_list):
        """Remove the longuest link in the list.

        Args:
            link_list (list of list of City): list of couple of connected City

        Returns:
            list of City: the couple of City to unlink
        """
        lmax = 0
        longest_link = list()
        for link in link_list:        
            l = link[0].dist2_to_city(link[1])
            if(l>lmax):
                lmax = l
                longest_link = link
        longest_link[0].remove_link_to_city(longest_link[1])
        return [longest_link[0],longest_link[1]]


    def _merge_cities(self, city_1, city_2):
        """Merge two cities: the merged city will be in the middle between city_1 and city_2.
        The merged city will be linked with every city that was linked with city_1 or city_2.
        If city_1 or city_2 is a capital and the merged city is a valid capital position, the merged city will
        be a capital.

        Args:
            city_1 (City)
            city_2 (City)
        """
        city_1.translate((city_2.x - city_1.x) // 2, (city_2.y - city_1.y) // 2)

        for adj_city_2 in city_2.adjacent_cities:
            if adj_city_2 != city_1 and not adj_city_2.is_adjacent_to(city_1):
                city_1.add_link_to_city(adj_city_2)

        city_2.remove_every_link()
        city_1.is_capital = (city_1.is_capital or city_2.is_capital) and city_1.is_valid_capital_position()
        self._cities.remove(city_2)


    def _remove_city(self, city):
        """Remove a city from the graph.

        Args:
            city (City): The City to remove.
        """
        for adj in city.adjacent_cities:
            adj.remove_link_to_city(city)        
        self._cities.remove(city)


    def _links_intersects(self, c1, c2, c3, c4):
        """Check if the link from c1 to c2 and the link from c3 to c4 does intersect.

        Args:
            c1 (City)
            c2 (City)
            c3 (City)
            c4 (City)

        Returns:
            bool: if the links intesects
        """
        # https://algorithmtutor.com/Computational-Geometry/Check-if-two-line-segment-intersect/

        def direction(p1,p2,p3):
            return (p3.x - p1.x) * (p2.y - p1.y) - (p2.x - p1.x) * (p3.y - p1.y) 

        def on_segment(p1, p2, p):
            return min(p1.x, p2.x) <= p.x <= max(p1.x, p2.x) and \
                min(p1.y, p2.y) <= p.y <= max(p1.y, p2.y)

        d1 = direction(c3, c4, c1)
        d2 = direction(c3, c4, c2)
        d3 = direction(c1, c2, c3)
        d4 = direction(c1, c2, c4)

        if ((d1 > 0 and d2 < 0) or (d1 < 0 and d2 > 0)) and \
            ((d3 > 0 and d4 < 0) or (d3 < 0 and d4 > 0)):
            return True

        elif d1 == 0 and on_segment(c3, c4, c1):
            return True
        elif d2 == 0 and on_segment(c3, c4, c2):
            return True
        elif d3 == 0 and on_segment(c1, c2, c3):
            return True
        elif d4 == 0 and on_segment(c1, c2, c4):
            return True
        else:
            return False


    def _adapt_capitals(self):
        """If a capital has an invalid position, change it to simple city and try to change one neighbour 
        to a capital.
        """
        capital_list = [city for city in self._cities if city.is_capital]
        for capital in capital_list:
            self._adapt_capital_position(capital)


    def _change_invalid_capitals_to_cities(self):
        """Change invalid capitals to simple cities.
        """
        capital_list = [city for city in self._cities if city.is_capital]
        for capital in capital_list:
            if not capital.is_valid_capital_position():
                capital.is_capital = False


    def _adapt_capital_position(self, capital):
        """If the capital has an invalid position, change it to simple city and try to change one neighbour 
        to a capital.
        
        Args:
            capital (City): the capital to adapt
        """
        if not capital.is_capital:
            return
        if capital.is_valid_capital_position():
            return

        capital.is_capital = False
        for adj in capital.adjacent_cities:
            if adj.is_valid_capital_position():
                adj.is_capital = True
                return            


    def _compute_cell_area(self, cell_cities):
        """Compute the area of a cell.

        Args:
            cell_cities (list of City): cities at the vertices of the cell

        Returns:
            float: the (algebric) area of the cell. The boundary cell has a negative area, others are positive.
        """
        #https://stackoverflow.com/questions/34326728/how-do-i-calculate-the-area-of-a-non-convex-polygon
        area = 0
        p1 = cell_cities[-1]
        for p2 in cell_cities:
            area += p1.y * p2.x - p1.x * p2.y
            p1 = p2
        return 0.5 * area


    def _compute_voronoi(self, map_width, map_height, margin):
        """Compute vornoi cells for each city

        Args:
            map_width (int): the width of the map
            map_height (int): the height of the map
            margin (int): number of pixels on the side of the map without any city
        """
        voronoi = Voronoi(self._cities, map_width, map_height, margin)
        self._voronoi_vertices = voronoi.coordinates
        self._voronoi_regions = voronoi.regions 



""" This module contains a random "Kingdom style" names generator.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""


import random
import re

class NameGenerator:
    """This class generates random city names and capital names without duplicates."""
    def __init__(self, language):
        """Init generator with specified language

        Args:
            language (string): choose between supported languages; each language uses spécific prefixes ans suffixes
        """
        if language == 'french':
            self._generator = FrenchGenerator()
        elif language == 'english':
            self._generator = EnglishGenerator()
        elif language == 'spanish':
            self._generator = SpanishGenerator()
        else:
            print(language + 'is not a valid language for names!')


    def gen_city_name(self):
        """Generate a city name. This name will be different than previously generated names.

        Returns:
            string: the generated city name
        """
        return self._generator.gen_city_name()

    def gen_capital_name(self):
        """Generate a capital name. This name will be different than previously generated names.

        Returns:
            string: the generated capital name
        """
        return self._generator.gen_capital_name()


class AbstractGenerator():
    """Each language must have a generator derived from this class.
    """
    def __init__(self):
        self._name_parts = dict()
        self._names = set()

    def _random_name_from_substrings(self, name_parts_keys):
        """Build a name concataining several substring. Each substring is randomly chosen in a list specified by
        a key passed in name_parts_keys. Each (by language) generator have its specific name_parts lists.
        For example, _random_name_from_substrings(["pref", "middle", suff]) will build a name from a 
        prefix chosen in "pref" list, a middle in "middle" list and a suffix in "suff" list.

        Args:
            name_parts_keys (list of string): the keys for lists of parts names.

        Returns:
            string: the generated name. It will be different than previously generated names.
        """
        name = ''
        while name == '' or name in self._names:
            name = ''
            for key in name_parts_keys:
                name += random.choice(self._name_parts[key])

        self._names.add(name)
        return name

    def _get_nb_names_from_substrings(self, name_parts_keys):
        """Tell how many different names you can build wiht name parts extrated from specified lists.

        Args:
            name_parts_keys (list of string): the keys for lists of parts names.

        Returns:
            int: the number of names that can be generated with these name_parts
        """
        nb_names = 1
        for key in name_parts_keys:
            nb_names *= len(self._name_parts[key])
        return nb_names     


class FrenchGenerator(AbstractGenerator):
    """Derived generator for french names.
    """
    def __init__(self):
        super().__init__()
        self._name_parts = { \
            'begin_1': ['Ag','Ang','Ann','Aux','Az','Bor','Cas','Ech','Erm','Gas','Gen','Is','Ker',\
                'Lom','Mar','Os','Par','Rag','Ros','Sig','Tor','Toul','Yer'],\
            'begin_2': ['Gui','Lu','Poi','Ra','Ro','Thi','Tri','Vau'],\
            'middle_1' : ['ail','arl','er', 'il', 'ir', 'om', 'ov', 'uch'],\
            'middle_2' : ['b','bar','ch','col','d','del','dol','f','l','m','mir','nast','nur','t'],\
            'end_1' : ['aigues','arde','ault','eigne','elme','elos','enay','esnil','euille','ieres',\
                'ignan','igny','igues','illac','illarde','oing','onne','onnet','ombes','oult','oute'],\
            'end_2' : ['boeuf','chon','court','cq','fitte','fried','gny','lan','llier','me','mois',\
                'moulin','nes','neuve','niere','nost','noy','rgy','ric','scon','ses','ssac','ssagne',\
                'tel','tin','vaux','viers','vors'],\
            'end_3parts' : ['ain','ald','and','as','aye','eil','ele','ert','es','ette','etz','eux',\
                'ild','ir','is','oi','os','our','oux','uch','ude','ur','us','uys','yac'],\
            'begin_cap_1' : ['Bon','Bour','Castel','Chapel','Chat','Chatel','For','Fort','Gran','Hers',\
                'Lac','Mont','Pal','Pont','Tour','Vil'],\
            'begin_cap_2' : ['Boi','Cha','Chante','Croi','Du','Guy','Ile','Palai','Pui','Roche','Ville'],\
            'cap_f_suffixes' : ['-la-Belle','-la-Bleue','-la-Coule','-la-Croix','-la-Gould','-la-Grande',\
                '-la-Hesse','-la-Magne','-la-Rose','-la-Rouge','-la-Tresne','-la-Mer'],\
            'cap_m_suffixes' : ['-le-Bec','-le-Bel','-le-Boeuf','-le-Comte','-le-Dor','-le-Ferrand',\
                '-le-Grand','-le-Ker','-le-Hault','-le-Pape','-le-Riche','-le-Vis'],\
            'cap_p_suffixes' : ['-les-Bains','-les-Bois','-les-Fermes','-les-Flots','-les-Mines',\
                '-les-Plages']}        


    def gen_all_city_names(self):
        """
        Generate all possible city names. Not very usefull, just for fun.
        """
        names_113 = [b + m + e \
            for b in self._name_parts['begin_1'] \
            for m in self._name_parts['middle_1'] \
            for e in self._name_parts['end_3parts']]
        names_223 = [b + m + e \
            for b in self._name_parts['begin_2'] \
            for m in self._name_parts['middle_2'] \
            for e in self._name_parts['end_3parts']]
        names_101 = [b + e \
            for b in self._name_parts['begin_1'] \
            for e in self._name_parts['end_1']]
        names_202 = [b + e \
            for b in self._name_parts['begin_2'] \
            for e in self._name_parts['end_2']]
        return names_113 + names_223 + names_101 + names_202


    def gen_city_name(self):
        """Generate a city name. This name will be different than previously generated names.

        Returns:
            string: the generated city name
        """
        nb_113_names = self._get_nb_names_from_substrings(['begin_1', 'middle_1', 'end_3parts'])
        nb_223_names = self._get_nb_names_from_substrings(['begin_2', 'middle_2', 'end_3parts'])
        nb_101_names = self._get_nb_names_from_substrings(['begin_1', 'end_1'])
        nb_202_names = self._get_nb_names_from_substrings(['begin_2', 'end_2'])


        name_type = random.randint(0, nb_113_names + nb_223_names + nb_101_names + nb_202_names - 1)
        if name_type < nb_113_names:
            return self._random_name_from_substrings(['begin_1','middle_1','end_3parts'])

        name_type -= nb_113_names
        if name_type < nb_223_names:
            return self._random_name_from_substrings(['begin_2','middle_2','end_3parts'])

        name_type -= nb_223_names
        if name_type < nb_101_names:
            return self._random_name_from_substrings(['begin_1','end_1'])

        return self._random_name_from_substrings(['begin_2','end_2'])


    def gen_capital_name(self):
        """Generate a capital name. This name will be a different name than those alrealdy generated.
        If the name use a "city style" basename, like "Angiruch" in capital name "Château-d'-Angiruch",
        it will be unique too.

        Returns:
            string: a name for a capital
        """

        rand_prefix = random.randint(0, 10)
        if rand_prefix == 0:
            return 'Château-des-' + self.gen_city_name()
        if rand_prefix == 1:
            name = self.gen_city_name()
            if(name[0] in ['A','E','I','O','U','Y']):
                return "Château-d'-" + name
            else:
                return "Château-du-" + name
        if rand_prefix == 2:
            return 'Les ' + self.gen_city_name() + self._gen_capital_suffix('n')
        if rand_prefix == 3:
            return 'Saint-' + self.gen_city_name() + self._gen_capital_suffix('m')
        if rand_prefix == 4:
            return 'Sainte-' + self.gen_city_name() + self._gen_capital_suffix(r'f')
        return self._gen_capital_basename() + self._gen_capital_suffix('n')


    def _gen_capital_basename(self):
        """Generate a basename for a capital. This basename will be different than previously generated names
        or basenames.

        Returns:
            string: the generated basename
        """
        nb_113_cap_basenames = self._get_nb_names_from_substrings(['begin_cap_1', 'middle_1', 'end_3parts'])
        nb_223_cap_basenames = self._get_nb_names_from_substrings(['begin_cap_2', 'middle_2', 'end_3parts'])
        name_type = random.randint(0, nb_113_cap_basenames + nb_223_cap_basenames - 1)

        if name_type < nb_113_cap_basenames:
            return self._random_name_from_substrings(['begin_cap_1', 'middle_1', 'end_3parts'])
        else:
            return self._random_name_from_substrings(['begin_cap_2', 'middle_2', 'end_3parts'])


    def _gen_capital_suffix(self, gender):
        """Generated a suffix for a capital name

        Args:
            gender (string): somme suffixes are gender dependant

        Returns:
            string: the generated suffix
        """
        rand_suffix = random.randint(0, 25)
        name = self.gen_city_name()
        if rand_suffix == 0 :
            if(name[0] in ['A','E','I','O','U','Y']):
                return "-d'" + name
            else:
                return '-de-' + name
        if rand_suffix == 1 :
            return '-des-' + name
        if rand_suffix == 2 :
            if(name[0] in ['A','E','I','O','U','Y']):
                return "-d'" + name
            else:
                return '-du-' + name
        if (rand_suffix == 3)and(gender != 'm'):
            if(name[0] in ['A','E','I','O','U','Y']):
                return "-l'" + name
            else:
                return '-la-' + name
        if (rand_suffix == 4)and(gender != 'f'):
            if(name[0] in ['A','E','I','O','U','Y']):
                return "-l'" + name
            else:
                return '-le-' + name        
        if rand_suffix == 5 :
            return '-les-' + name
        if rand_suffix == 6 :
            return '-sur-' + name
        if rand_suffix < 20 :
            suffixes = self._name_parts['cap_p_suffixes']
            if(gender != 'f'):
                suffixes = suffixes + self._name_parts['cap_m_suffixes']
            if(gender != 'm'):
                suffixes = suffixes + self._name_parts['cap_f_suffixes']
            return random.choice(suffixes)
        return ''


class EnglishGenerator(AbstractGenerator):
    """Derived generator for english names.
    """
    def __init__(self):
        super().__init__()
        self._name_parts = { \
            'begin_1': ['Aber','Ag','Aln','Alver','Alvin','Barn','Basing','Cam','Der','Inver','Is',\
                'Kil','Liver','Oln','Os','Osw','Pen','Read','Rom','Swin','Tam','Ulg','Win'],\
            'begin_2': ['Ave','Beau','Berke','Kili','Lei','Plai'],\
            'middle_1': ['ail','arl','er','il','ir','om','ov','uch'],\
            'middle_2': ['bar','b','ch','col','d','del','dol','f','l','m','mir','nast','nur','t'],\
            'end_1': ['aton','enay','ingdon','ingham','ingley','ington','inster','orley','iton',\
                'ompton','orough','oughton','uton'],\
            'end_2': ['bank','barton','berg','bergh','bigh','bley','borough','bridge','broke','brook',\
                'bury','canton','caster','cester','chet','church','coed','combe','connaugh','connell',\
                'cot','cote','cott','court','cross','cy','dean','den','ditch','dover','field','fleet',\
                'ford','glen','gordon','hall','ham','hampton','haven','head','hill','holm','holme',\
                'holt','house','hurst','ley','loch','low','mally','manston','market','martin','mere',\
                'minster','mond','more','mouth','nash','nauld','nell','ness','ney','nock','pool','port',\
                'sea','shall','sham','shaw','sop','sor','stam','stead','stoft','stoke','ston','stone',\
                'stow','stowe','sutton','town','vale','vern','villy','wald','well','wich','wick','wold',\
                'worth'],\
            'end_3': ['ald','all','am','and','ary','ave','by','eld','enham','ere','ert','esey','eton',\
                'ey','ing','iton','on','ord','orne','ough','our','over'],\
            'cap_begin_1': ['Aber','Abing','Accring','Act','Alder','Amer','Ames','And','Bally','Bar',\
                'Barn','Basil','Basing','Beacon','Billing','Birm','Black','Brack','Brad','Brent','Camel',\
                'Carn','Castle','Chat','Chelms','Ciren','Col','Cran','Craw','Crow','Croy','Cumber','Dart',\
                'Den','Dul','Dun','Dur','East','Elles','Ep','Fair','Fal','Farn','Faver','Fern','Finch',\
                'Glaston','Gos','Green','Guil','Hales','Harrow','Hart','Hast','Hol','Holy','Inver','Kil',\
                'Killy','Kings','Kirk','Led','Led','Letch','Liver','Long','Lough','Maccles','Man','Mount',\
                'Nail','Nar','New','North','Old','Pen','Peter','Port','Portis','Queen','Rath','Read',\
                'Red','Rich','Rip','Roch','Rother','Rush','Sand','Scar','Scun','Shif','Shrew','South',\
                'Stain','Stal','Staly','Stam','Stour','Tar','Tel','Under','Watling','West','York'],\
            'cap_begin_2': ['Bide', 'Bourne','Brau','Clee','Cleo','Cleve','Clithe','Cole','Dere','Hale',\
                'Harro','Hartle','Knare','Launce','Little','Pole','Shafte','Shire','Slea','Stone','Yeo']}


    def gen_city_name(self):
        """Generate a city name. This name will be different than previously generated names.

        Returns:
            string: the generated city name
        """
        nb_113_names = self._get_nb_names_from_substrings(['begin_1', 'middle_1', 'end_3'])
        nb_223_names = self._get_nb_names_from_substrings(['begin_2', 'middle_2', 'end_3'])
        nb_101_names = self._get_nb_names_from_substrings(['begin_1', 'end_1'])
        nb_202_names = self._get_nb_names_from_substrings(['begin_2', 'end_2'])

        name_type = random.randint(0, nb_113_names + nb_223_names + nb_101_names + nb_202_names - 1)
        if name_type < nb_113_names:
            return self._random_name_from_substrings(['begin_1','middle_1','end_3'])

        name_type -= nb_113_names
        if name_type < nb_223_names:
            return self._random_name_from_substrings(['begin_2','middle_2','end_3'])

        name_type -= nb_223_names
        if name_type < nb_101_names:
            return self._random_name_from_substrings(['begin_1','end_1'])

        return self._random_name_from_substrings(['begin_2','end_2'])


    def gen_capital_name(self):
        """Generate a capital name. This name will be different than previously generated names.

        Returns:
            string: the generated capital name
        """
        nb_cap_11_names = self._get_nb_names_from_substrings(['cap_begin_1', 'end_1'])
        nb_cap_12_names = self._get_nb_names_from_substrings(['cap_begin_1', 'end_2'])
        nb_cap_13_names = self._get_nb_names_from_substrings(['cap_begin_1', 'end_3'])
        nb_cap_22_names = self._get_nb_names_from_substrings(['cap_begin_2', 'end_2'])

        name_type = random.randint(0, nb_cap_11_names + nb_cap_12_names + nb_cap_13_names + nb_cap_22_names - 1)
        if name_type < nb_cap_11_names:
            return self._random_name_from_substrings(['cap_begin_1', 'end_1'])

        name_type -= nb_cap_11_names
        if name_type < nb_cap_12_names:
            return self._random_name_from_substrings(['cap_begin_1', 'end_2'])

        name_type -= nb_cap_12_names
        if name_type < nb_cap_13_names:
            return self._random_name_from_substrings(['cap_begin_1', 'end_3'])

        return self._random_name_from_substrings(['cap_begin_2', 'end_2'])


class SpanishGenerator(AbstractGenerator):
    """Derived generator for spanish names.
    """
    def __init__(self):
        super().__init__()
        self._name_parts = { \
            'begin': ['Ael','Alc','Ant','Ben','Bes','Bol','Castel','Cer','Chas','Cob','Def','Dom','Ecr',\
                'Fam','Ferr','Fort','Gard','Gen','Gran','Herb','Hoz','Huy','Is','Jos','Kel','Kym','Lord',\
                'Mal','Noj','Nuh','Ogr','Ont','Palm','Part','Pel','Pin','Pont','Tor','Tuk','Tys','Urb',\
                'Url','Val','Yur'],\
            'middle' : ['ac','af','az','ech','en','er','if','in','iz','on','oy','ub','um'],\
            'end' : ['ald','ar','ás','at','ato','éjar','ela','eld','en','etas','eto','ey','ez',\
                'ia','igas','illo','ix','old','ón','ost','oz','um','uto','uz','yd'],\
            'begin_cap_San' : ['Al','Amo','An','Be','Ci','Da','De','Ero','Fo','Ga','Gue','He','Ho',\
                'Ino','Ju','Lu','Me','No','Obe','Po','Ro','Ru','So'],\
            'middle_cap_San' : ['c','f','l','n','r','t','tes','ton','x'],\
            'end_cap_San' : ['án','ancio','ando','ario','edo','eo','és',\
                'erto','iano','iel','ildo','io','ión','ín','uno'],\
            'begin_cap_f' : ['Agua','Alba','Anda','Brisa','Cara','Colina','Cuesta','Cueva','Espina',\
                'Estepa','Fuente','Gama','Gota','Jara','Ladera','Lira','Loma','Meda','Moza','Palma',\
                'Peña','Piedra','Pinta','Quinta','Ribera','Riva','Roca','Sierra','Tierra','Torre','Tru',\
                'Villa','Vista'],\
            'begin_cap_m' : ['Alcon','Bosque','Burgo','Campo','Canto','Caste','Cielo','Conde','Fasto',\
                'Fray','Fresno','Heno','Hierro','Humo','Leon','Llano','Loto','Mato','Molino','Monte',\
                'Muro','Naranjo','Nido','Nogal','Olivo','Pasto','Pinar','Puente','Rio','Roble','Oso',\
                'Pozo','Prado','Toro'],\
            'end_cap_f' : ['bella','blanca','buena','clara','cruz','fiel','franca','hueca','jillón',\
                'lejana','lisa','lta','lupo','nueva','negra','magna','marca','mendi','pura','quebrada',\
                'reina','ria','rica','vieja','villa','vértiz','zul'],\
            'end_cap_m' : ['agudo','alegre','alto','bello','blanco','bravo','cortés','dena','duro',\
                'fuerte','fé','gana','güén','hermoso','lares','lid','mar','negro','nuevo','patrón','puro',\
                'real','rebelde','sacro','santo','seco','sereno','viejo'],\
            'end_cap_n' : ['flores','grande','lén','libre','mayor','noble']}


    def gen_city_name(self):
        """Generate a city name. This name will be different than previously generated names.

        Returns:
            string: the generated city name
        """
        return self._random_name_from_substrings(['begin','middle','end'])

    def gen_capital_name(self):
        """Generate a capital name. This name will be different than previously generated names.

        Returns:
            string: the generated capital name
        """
        rand_type = random.randint(0, 10)
        if rand_type == 0:
            return self.gen_city_name()

        elif rand_type <3:
            return 'San ' + self._random_name_from_substrings(['begin_cap_San','middle_cap_San','end_cap_San'])

        else:
            nb_ff_names = self._get_nb_names_from_substrings(['begin_cap_f', 'end_cap_f'])
            nb_fn_names = self._get_nb_names_from_substrings(['begin_cap_f', 'end_cap_n'])
            nb_mm_names = self._get_nb_names_from_substrings(['begin_cap_m', 'end_cap_m'])
            nb_mn_names = self._get_nb_names_from_substrings(['begin_cap_m', 'end_cap_n'])

            n = random.randint(0, nb_ff_names + nb_fn_names + nb_mm_names + nb_mn_names - 1)

            if n < nb_ff_names:
                return self._random_name_from_substrings(['begin_cap_f', 'end_cap_f'])

            n -= nb_ff_names
            if n < nb_fn_names:
                return self._random_name_from_substrings(['begin_cap_f', 'end_cap_n'])
            
            n -= nb_fn_names
            if n < nb_mm_names:
                return self._random_name_from_substrings(['begin_cap_m', 'end_cap_m'])

            return self._random_name_from_substrings(['begin_cap_m', 'end_cap_n'])


"""
Not realy a part of the Map Generator. This module contains tools to generate dummy kingdoms
in order to test map rendering.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import random
import json
from mapgraph import MapGraph
import mapanalysis


medieval_names = ['Abélard','Albéric','Amaury','Ambroise','Amédée','Anselme','Archibald',\
    'Aymar','Aymon','Baudouin','Béranger','Bernard','Bertrand','Bohémond','Charles','Clavin',\
    'Clérambault','Come','Edouard','Enguerrand','Estienne','Eudes','Eustache','Fernand','Firmin',\
    'Foulques','Gaspard','Gautier','Geoffroy','Georges','Gilbert','Godefroy','Guilhem','Henri',\
    'Herman','Hugues','Imbault','Jacquemard','Jean','Jehan','Louis','Luquin','Maximilien','Othon',\
    'Pélage','Perrin','Philibert','Pons','Raimond','Renaud','Roland','Rodolphe','Sigismond','Théodore',\
    'Tristan','Ymbert','Adélaïde','Aliénor','Agnès','Alix','Anne','Azalaïs','Béatrice','Bénédicte',\
    'Bérengère','Blanche','Brunissende','Catherine','Chimène','Célie','Clarence','Constance','Cornélie',\
    'Eléanore','Emmerance','Ermangarde','Esclarmonde','Flore','Francine','Guillemine','Hélène','Héloïse',\
    'Hermance','Hildegarde','Isabeau','Iseult','Jeanne','Louise','Madeleine','Mahaut','Margot',\
    'Mélisande','Perrine','Pétronille','Sibylle','Suzanne','Ysabel','Yolande']


class KingdomsGenerator:
    """A class to generate dummy kingdoms
    """
    def __init__(self, map_graph, nb_players, max_dist_to_capital):
        """Init the class an populate the dummy kingoms

        Args:
            map_graph (MapGraph): the graph of the map we want to populate
            nb_players (int): number of kingdoms to generate
            max_dist_to_capital (int): maximum distance between a capital of a kingdom and its other cities
        """
        self._map_graph = map_graph
        self._city_to_player = dict()
        for city in map_graph.cities:
            self._city_to_player[city] = -1

        self._place_capitals(nb_players)
        self._expand_kingdoms(max_dist_to_capital)


    def export_to_json(self, jsfile):
        """Export the kingdoms in a json file

        Args:
            jsfile (string): json file name
        """
        with open(jsfile, 'w') as outfile:
            gameState = dict()
            gameState["players"] = self._player_names
            #gameState["cities"] = self._city_to_player.values()
            gameState["cities"] = [self._city_to_player[city] for city in self._map_graph.cities]
            outfile.write(json.dumps(gameState, ensure_ascii=False))


    def _place_capitals(self, nb_players):
        """Give each player a capital

        Args:
            nb_players (int): Number of players (=kingdoms in the map
        """
        capital_list = [city for city in self._map_graph.cities if city.is_capital]
        capitals = random.sample(capital_list, nb_players)
        self._player_names = random.sample(medieval_names, nb_players)

        self._player_to_cities = dict()
        for i, capital in enumerate(capitals):
            self._player_to_cities[i] = [capital]
            self._city_to_player[capital] = i


        
    def _expand_kingdoms(self, max_dist_to_capital):
        """Add cities to each kingdom until reaching max_dist_to_capital

        Args:
            max_dist_to_capital (int): maximum distance between a capital of a kingdom and its other cities
        """
        for _ in range(max_dist_to_capital):
            conquered_cities = list()

            for player, cities in self._player_to_cities.items():
                for city in cities:
                    for adj_city in city.adjacent_cities:
                        if self._city_to_player[adj_city] == -1:
                            self._city_to_player[adj_city] = player
                            conquered_cities.append(adj_city)

            for city in conquered_cities:
                self._player_to_cities[self._city_to_player[city]].append(city)



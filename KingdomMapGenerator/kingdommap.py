"""
This module implements the KingdomMap class, the main class of the map generator.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import math
import random
import json

from mapgraph import MapGraph
from mapcell import MapCell
from mapchunks import MapChunks
from spritemanager import SpriteManager



class KingdomMap:
    """KingdomMap is the class that represents a map in Kingdom.
    """
    def __init__(self, width, height):
        """Create an empty map

        Args:
            width (int): the width of the map in pixels
            height (int): the height of the map in pixels
        """
        self._width = width
        self._height = height
        self._map_graph = MapGraph()


    def generate_graph(self, gen_params):
        """Generate the map graph: the cities position and the links between cities.

        Args:
            gen_params (dict): generation parameters.
        """
        self._map_graph.generate(self._width, self._height, gen_params)


    def load_graph_from_json(self, json_filename, language, map_margin):
        """Load a map graph from a json file.

        Args:
            json_filename (string): path to the json file defining the map graph
            language (string): map language, needed for cities name generation
            map_margin (int): the margin in pixels on the sides of the map
        """
        with open(json_filename) as f:
            cities_data_list = json.load(f)
        self._map_graph.load(cities_data_list, language, self._width, self._height, map_margin)


    def generate_landscape(self, sprites_filename, landforms_filename):
        """Place images to generate a nice landscape

        Args:
            sprites_filename (string): json file with the position of each sprites in spritesheets
            landforms_filename (string): json file describing how to use sprites to generate landforms
        """
        self._sprites_manager = SpriteManager(sprites_filename)
        self._map_chunks = MapChunks(512, self._width, self._height)
        self._fill_cells_with_images(landforms_filename)


    @property
    def width(self):
        """
        Returns:
            int: the width of the map in pixels
        """
        return self._width

    @property
    def height(self):
        """
        Returns:
            int: the heigth of the map in pixels
        """
        return self._height

    @property
    def map_graph(self):
        """
        Returns:
            MapGraph: the map graph associated to the map
        """
        return self._map_graph

    @property
    def map_chunks(self):
        """
        Returns:
            MapChunks: the map chunks associated to the map
        """
        return self._map_chunks


    def export_graph(self, json_file_name):
        """Export the map graph into a json file.

        Args:
            json_file_name (string): path to the json file
        """
        serial = self._map_graph.export_cities()
        with open(json_file_name, 'w') as outfile:
            outfile.write(json.dumps(serial, ensure_ascii=False, separators=(',', ':')))        


    def export_render_data(self, json_file_name):
        """Export the data to render the map (images positions, voronoi cells coordinates, etc)
        into a json file

        Args:
            json_file_name (string): path to the json file
        """
        with open(json_file_name, 'w') as outfile:
            outfile.write(json.dumps({ \
                "map_width": self.width, \
                "map_height": self.height, \
                "cities_sprites": self._sprites_manager.export_cities_sprites_definition(), \
                "sprites": self._sprites_manager.export_sprites_definition(), \
                "htiles": self._sprites_manager.export_htiles_definition(), \
                "map_chunks": self._map_chunks.export(), \
                "paths": self._map_graph.get_paths_lines_to_draw(), \
                "voronoi_vertices": self._map_graph.voronoi_vertices, \
                'voronoi_regions_vert': [region["vertices"] for region in self._map_graph.voronoi_regions.values()], \
                'voronoi_regions_adj': [region["adj_regions"] for region in self._map_graph.voronoi_regions.values()]},\
                separators=(',', ':')))


    def _fill_cells_with_images(self, landforms_filename):
        """Place landscape images in the map cells. Map cells are part of the map delimited by paths.

        Args:
            landforms_filename (string): path to a json file with landforms parameters
        """
        with open(landforms_filename) as f:
            data = json.load(f)
            htiles_data = data["htiles"]
            landforms_data = data["landforms"]


        for htiles_dict in htiles_data:
            self._sprites_manager.gen_htiles_set(htiles_dict)


        cell_cities_list = self._map_graph.find_cells()
        nb_cells = len(cell_cities_list)
        nb_mountains_cells = random.randint(math.floor(nb_cells * 0.01), math.floor(nb_cells * 0.03))


        for cell_cities in cell_cities_list[nb_mountains_cells:-1]:
            cell = MapCell(cell_cities, self._width, self._height)
            dice = random.randint(1,12)
            nb_cities = len(cell_cities)

            if nb_cities == 3:
                if dice <=8:
                    landform = 'dirt'
                elif dice <=10:
                    landform = 'hills'
                else:
                    landform = 'forest'
            elif nb_cities == 4:
                if dice <= 4:
                    landform = 'dirt'
                elif dice <= 6:
                    landform = 'hills'
                elif dice <= 8:
                    landform = 'forest'
                else:
                    landform = 'lake' 
            else:
                if dice <= 4:
                    landform = 'hills'
                elif dice <= 8:
                    landform = 'forest'
                elif dice <= 11:
                    landform = 'lake'
                else:
                    landform = 'sand'

            cell.fill_with_sprites(landforms_data[landform], self._sprites_manager, False)
            self._map_chunks.add_images_from_cell(cell)

        boundary_cell = MapCell(cell_cities_list[-1], self._width, self._height)
        boundary_cell.fill_with_sprites(landforms_data["mountains"], self._sprites_manager, True)
        self._map_chunks.add_images_from_cell(boundary_cell)

        for cell_cities in cell_cities_list[:nb_mountains_cells]:
            cell = MapCell(cell_cities, self._width, self._height)
            mountain_type = random.randint(1,4)
            if mountain_type < 3:
                cell.fill_with_sprites(landforms_data["mountains"], self._sprites_manager, False)
            elif mountain_type == 3:
                cell.fill_with_sprites(landforms_data["volcanos"], self._sprites_manager, False)
            else:
                cell.fill_with_sprites(landforms_data["snowy_mountains"], self._sprites_manager, False)
            self._map_chunks.add_images_from_cell(cell)


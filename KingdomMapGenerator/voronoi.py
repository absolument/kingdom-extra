"""
Extract Voronoi region from Delaunay triangulation using Bowyer-Watson algorithm
Based on codes from Ayron Catteau ( http://github.com/ayron/delaunay )
and Jose M. Espadero ( http://github.com/jmespadero/pyDelaunay2D )

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""


class Triangle:
    def __init__(self, v0, v1, v2, coords_table):
        """Create a tringle from its vertices

        Args:
            v0 (int): index of first triangle vertex
            v1 (int): index of second triangle vertex
            v2 (int): index of third triangle vertex
            coords_table (dict of int: list of float): table of coordinates
        """
        self.v = [v0,v1,v2]
        self.neighbour = [None, None, None]

        v0x = coords_table[v0][0]
        v0y = coords_table[v0][1]
        v1x = coords_table[v1][0]
        v1y = coords_table[v1][1]
        v2x = coords_table[v2][0]
        v2y = coords_table[v2][1]

        v0_norm2 = v0x**2 + v0y**2
        v1_norm2 = v1x**2 + v1y**2
        v2_norm2 = v2x**2 + v2y**2
        d = 2 * (v0x * (v1y - v2y) + v1x * (v2y - v0y) + v2x * (v0y - v1y))

        cx = (v0_norm2 * (v1y - v2y) + v1_norm2 * (v2y - v0y) + v2_norm2 * (v0y - v1y)) / d
        cy = (v0_norm2 * (v2x - v1x) + v1_norm2 * (v0x - v2x) + v2_norm2 * (v1x - v0x)) / d

        self._circumcenter = [cx,cy]
        self._radius_2 = (v0x - cx)**2 + (v0y - cy)**2


    @property
    def circumcenter(self):
        """
        Returns:
            list of float: coordinates of the triangle circumcenter
        """
        return self._circumcenter

    def is_in_circumcircle(self, point):
        dist_2 = (point[0] - self._circumcenter[0])**2 + (point[1] - self._circumcenter[1])**2
        return dist_2 <= self._radius_2



class Delaunay:
    def __init__(self, map_width, map_height):
        """Init delaunay triangulation with the four corners of a rectangle.
        Up left corner coordinates: (0,0)
        Bottom right corner coordinates: (map_width, map_height)

        Args:
            map_width (float): rectangle width
            map_height (float): rectangle height
        """
        self._coords = {-1:[0,0], -2:[0,map_height], -3:[map_width, map_height], -4:[map_width, 0]}
        
        T1 = Triangle(-1,-2,-4, self._coords)
        T2 = Triangle(-3,-4,-2, self._coords)
        T1.neighbour[0] = T2
        T2.neighbour[0] = T1
        self._triangles = [T1,T2]

    @property
    def coords(self):
        """
        Returns:
            dict of int: list of float: list of the couples of coordinates for each point
        """
        return self._coords

    @property
    def triangles(self):
        """
        Returns:
           list of Triangle: Delaunay triangulation
        """
        return self._triangles


    def add_point(self, city_id, point):
        """Add a point using Bowyer-Watson algorithm

        Args:
            city_id (int): The point id
            point (list of float): couple of coordinates of the point
        """
        self._coords[city_id] = point

        # search the triangle(s) whose circumcircle contains p
        bad_triangles = list()
        for triangle in self._triangles:
            if triangle.is_in_circumcircle(point):
                bad_triangles.append(triangle)

        bad_triangles_boundary = self._boundary(bad_triangles)

        for tri in bad_triangles:
            self._triangles.remove(tri)

        # retriangle the hole left by bad_triangles
        new_triangles = []
        for (e0, e1, opp_tri) in bad_triangles_boundary:
            tri = Triangle(city_id, e0, e1, self._coords)
            tri.neighbour[0] = opp_tri

            if opp_tri:
                # search the neighbour of opp_tri that use edge (e1, e0)
                for i, neigh in enumerate(opp_tri.neighbour):
                    if neigh:
                        if e1 in neigh.v and e0 in neigh.v:
                            opp_tri.neighbour[i] = tri
            new_triangles.append(tri)

        nb_new_tri = len(new_triangles)
        for i, tri in enumerate(new_triangles):
            tri.neighbour[2] = new_triangles[(i-1) % nb_new_tri]
            tri.neighbour[1] = new_triangles[(i+1) % nb_new_tri]
   
        self._triangles.extend(new_triangles)   


    def _boundary(self, triangles):
        """ Search the convex hull of a list of adjacent triangles
        expressed as a list of edges (point pairs) and the opposite triangle
        in ccw order

        Args:
            triangles (list of Triangle): we search the boundary of this adjacent triangle list

        Returns:
            list of (int,int,int): convex hull
        """
        boundary = list()
        tri = triangles[0]
        edge = 0

        while True:
            if len(boundary) > 1:
                if boundary[0] == boundary[-1]:
                    break

            if tri.neighbour[edge] in triangles:
                last = tri
                tri = tri.neighbour[edge]
                edge = (tri.neighbour.index(last) + 1) % 3 

            else: 
                boundary.append((tri.v[(edge+1)%3], tri.v[(edge+2)%3], tri.neighbour[edge]))
                edge = (edge + 1) % 3

        return boundary[:-1]



class Voronoi:
    """ Voronoi diagram for strategic view of the map:
    the cells are clamped at the egde of the map.
    """

    def __init__(self, cities, map_width, map_height, margin):
        """Create the Voronoi diagram

        Args:
            cities (list of city): these cities are an output from map generation
            map_width (int): the width of the map
            map_height (int): the height of the map
            margin (int): the distance from the edges of the map to clamp the Voronoi cells 
        """
        self._delaunay = Delaunay(map_width, map_height)
        #for city_id, city in cities.items():
        #    self._delaunay.add_point(city_id, [city['x'], city['y']])
        for i, city in enumerate(cities):
            self._delaunay.add_point(i, [city.x, city.y])

        self._vor_coords = []
        self._use_vertex = {i: [] for i in self._delaunay.coords.keys()}
        self._index = {}

        # Build a list of coordinates and one index per triangle/region
        for triangle_id, triangle in enumerate(self._delaunay.triangles):
            (a, b, c) = triangle.v
            circumcenter = triangle.circumcenter
            self._vor_coords.append([int(round(circumcenter[0])), int(round(circumcenter[1]))])
            # Insert triangle, rotating it so the key is the "last" vertex
            self._use_vertex[a] += [(b, c, a)]
            self._use_vertex[b] += [(c, a, b)]
            self._use_vertex[c] += [(a, b, c)]
             # Set triangle_id as the index to use with this triangle
            self._index[(a, b, c)] = triangle_id
            self._index[(c, a, b)] = triangle_id
            self._index[(b, c, a)] = triangle_id

        self._regions = {}
        # Sort each region in a coherent order, and substitude each triangle by its index
        for i in self._delaunay.coords.keys():
            if i<0:
                continue
            v = self._use_vertex[i][0][0] # Get a vertex of a triangle
            region = []
            adj_regions = []
            for _ in range(len(self._use_vertex[i])):
                # Search the triangle beginning with vertex v
                t = [t for t in self._use_vertex[i] if t[0] == v][0]
                region.append(self._index[t])
                v = t[1]
                adj_regions.append(v)
            self._regions[i] = {}
            self._regions[i]["vertices"] = region
            self._regions[i]["adj_regions"] = adj_regions

        self._clamp_regions_to_margin(map_width, map_height, margin)
        self._remove_out_of_map_vertices(map_width, map_height, margin)


    @property
    def regions(self):
        """
        Returns:
            dict: keys are cities ids.
            Values contains the list of vertices, and the list of adjacent regions (identified by their city id)
        """
        return self._regions

    @property
    def coordinates(self):
        """
        Returns:
            list of list of int: coordinates of the vertices of voronoi cells, rounded to integers
        """
        return self._vor_coords


    def _clamp_regions_to_margin(self, map_width, map_height, margin):
        """Clamp Voronoi cell to fit the map rectangle with margins

        Args:
            map_width (int): the width of the map
            map_height (int): the height of the map
            margin (int): the distance from the edges of the map to clamp the Voronoi cells 
        """
        dict_ids_boundary = dict()

        for i, region in self._regions.items():
            new_region_vertices = []
            new_adj_region = []

            v1 = region["vertices"][-2]
            v2 = region["vertices"][-1]

            for j, v3 in enumerate(region["vertices"]):
                if self._is_in_map(v2, map_width, map_height, margin):
                    new_region_vertices.append(v2)
                    new_adj_region.append(region["adj_regions"][j-1])
                else:
                    if self._is_in_map(v1, map_width, map_height, margin):
                        if (v1,v2) not in dict_ids_boundary:
                            self._add_vertex_on_map_boundary(v1,v2,map_width,map_height, margin, dict_ids_boundary)
                        v = dict_ids_boundary[(v1,v2)]
                        new_region_vertices.append(v)
                        new_adj_region.append(-1)

                    if self._is_in_map(v3, map_width, map_height, margin):
                        if len(new_region_vertices):
                            x = self._vor_coords[new_region_vertices[-1]][0]
                            y = self._vor_coords[new_region_vertices[-1]][1]

                            #if the cell is in the map corner, we need to add a vertex
                            corner = False
                            if y == margin and not self._is_x_in_map(v2, map_width, margin):
                                self._vor_coords.append([margin,margin]) #up left corner
                                corner = True
                            elif x == margin and not self._is_y_in_map(v2, map_height, margin):
                                self._vor_coords.append([margin, map_height - margin]) #down left corner
                                corner = True
                            elif y == map_height - margin and not self._is_x_in_map(v2, map_width, margin):
                                self._vor_coords.append([map_width - margin, map_height - margin]) #down right corner
                                corner = True
                            elif x == map_width - margin and not self._is_y_in_map(v2, map_height, margin):
                                self._vor_coords.append([map_width - margin,margin]) #up right corner
                                corner = True

                            if corner:
                                corner_id = len(self._vor_coords) - 1
                                new_region_vertices.append(corner_id)
                                new_adj_region.append(-1)                                


                        if (v3,v2) not in dict_ids_boundary:
                            self._add_vertex_on_map_boundary(v3,v2,map_width,map_height, margin, dict_ids_boundary)
                        v = dict_ids_boundary[(v3,v2)]
                        new_region_vertices.append(v)                        
                        new_adj_region.append(region["adj_regions"][j-1])
                v1 = v2
                v2 = v3


            self._regions[i]["vertices"] = new_region_vertices
            self._regions[i]["adj_regions"] = new_adj_region

    def _remove_out_of_map_vertices(self, map_width, map_height, margin):
        """Clean list of Voronoi vertices list to remove out of map (unused) vertices

        Args:
            map_width (int): the width of the map
            map_height (int): the height of the map
            margin (int): the distance from the edges of the map to clamp the Voronoi cells 
        """
        vor_coords = list()
        offsets = list()

        current_offset = 0
        for i,v in enumerate(self._vor_coords):
            offsets.append(current_offset)
            if self._is_in_map(i, map_width, map_height, margin):
                vor_coords.append(v)
            else:
                current_offset -= 1

        self._vor_coords = vor_coords

        for region in self._regions.values():
            for i,v in enumerate(region["vertices"]):
                region["vertices"][i] += offsets[v]


    def _is_x_in_map(self, vor_coord_id, map_width, margin):
        """Check if x coordinates of a Voronoi vertex is in map x range

        Args:
            vor_coord_id (int): the index of the vertex in self._vor_coords
            map_width (int): the width of the map
            margin (int): the distance from the edges of the map to clamp the Voronoi cells 

        Returns:
            [type]: [description]
        """
        return (margin <= self._vor_coords[vor_coord_id][0]) and \
               (self._vor_coords[vor_coord_id][0] <= map_width - margin)

    def _is_y_in_map(self, vor_coord_id, map_height, margin):
        """Check if y coordinates of a Voronoi vertex is in map y range

        Args:
            vor_coord_id (int): the index of the vertex in self._vor_coords
            map_height (int): the height of the map
            margin (int): the distance from the edges of the map to clamp the Voronoi cells 

        Returns:
            [type]: [description]
        """
        return (margin <= self._vor_coords[vor_coord_id][1]) and \
               (self._vor_coords[vor_coord_id][1] <= map_height - margin)

    def _is_in_map(self, vor_coord_id, map_width, map_height, margin):
        """Check if a Voronoi vertex is in map area

        Args:
            vor_coord_id (int): the index of the vertex in self._vor_coords
            map_width (int): the width of the map
            map_height (int): the height of the map
            margin (int): the distance from the edges of the map to clamp the Voronoi cells 

        Returns:
            [type]: [description]
        """
        return self._is_x_in_map(vor_coord_id, map_width, margin) and \
               self._is_y_in_map(vor_coord_id, map_height, margin)


    def _add_vertex_on_map_boundary(self, in_map_id, out_map_id, map_width, map_height, margin, dict_ids_boundary):
        """Add a vertex at the intesection of the edge of a Voronoi cell and the edges of the map
        Args:
            in_map_id (int): index of the edge vertex which is in the map area
            out_map_id (int): index of the edge vertex which is out of the map area
            map_width (int): the width of the map
            map_height (int): the height of the map
            margin (int): the distance from the edges of the map to clamp the Voronoi cells 
            dict_ids_boundary (dict): Keys are added on map boundary vertices.
                Values are the list of concerned egdes.
        """
        v1 = self._vor_coords[in_map_id]
        v2 = self._vor_coords[out_map_id]
        a = v2[1] - v1[1]
        b = v1[0] - v2[0]
        c = v2[0] * v1[1] - v1[0] * v2[1]
        #equation of the line that passes throught v1 and v2 is a * x + b * y + c = 0

        if v2[0] < margin:
            x = margin
            y = -(c + a * margin) // b
        elif v2[0] > map_width - margin:
            x = map_width - margin
            y = -(c + a * (map_width - margin)) // b
        elif v2[1] < margin:
            y = margin
            x = -(c + b * margin) // a
        elif v2[1] > map_height - margin:
            y = map_height - margin
            x = -(c + b * (map_height - margin)) // a
        else:
            #shoudn't go there
            print("v2 is in map area!")

        new_vor_vertex_id = len(self._vor_coords)
        self._vor_coords.append([x,y])
        dict_ids_boundary[(in_map_id, out_map_id)] = new_vor_vertex_id


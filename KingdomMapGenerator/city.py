"""
This module implements a City class for Kingdom map generator.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import math


class City:
    """A city on the map.
    """
    def __init__(self, x, y, name_gen, capital = False):
        """Create a City

        Args:
            x (int): x position in pixels of the city
            y (int): y position in pixels of the city
            name_gen (NameGenerator): a name generator to give kingdomish name to the city.
            capital (bool, optional): is the city a capital. Defaults to False.
        """
        self._x = x
        self._y = y
        self._is_capital = capital
        self._adjacent_cities = list()
        self._name_gen = name_gen
        if capital:
            self._name = name_gen.gen_capital_name()
        else:
            self._name = name_gen.gen_city_name()

    @property
    def x(self):
        """
        Returns:
            int: x position in pixels of the city
        """
        return self._x

    @property
    def y(self):
        """
        Returns:
            int: x position in pixels of the city
        """
        return self._y

    @property
    def is_capital(self):
        """
        Returns:
            bool: is the city a capital, or a simple city
        """
        return self._is_capital

    @is_capital.setter
    def is_capital(self, value):
        """
        Args:
            value (bool): set capital
        """
        if value != self._is_capital:
            if value:
                self._name = self._name_gen.gen_capital_name()
            else:
                self._name = self._name_gen.gen_city_name()            
        self._is_capital = value

    @property
    def name(self):
        """
        Returns:
            string: the name of the city
        """
        return self._name

    @name.setter
    def name(self, value):
        """Set a name to the city
        Args:
            value (string): name
        """
        self._name = value

    @property
    def adjacent_cities(self):
        """
        Returns:
            list of City: the list of the other cities which are linked to the City
        """
        return self._adjacent_cities

    
    def translate(self, dx, dy):
        """Translate city position

        Args:
            dx (int): x axis translation in pixels
            dy (int): y axis translation in pixels
        """
        self._x += dx
        self._y += dy


    def is_adjacent_to(self, city):
        """Says if there is a link between the city and an other city

        Args:
            city (City): the other city

        Returns:
            bool: is there a link between the cities
        """
        return city in self._adjacent_cities


    def add_link_to_city(self, city):
        """Add a link, in both sides, between the city and another city

        Args:
            city (City): the city to link
        """
        if not self.is_adjacent_to(city):
            self._adjacent_cities.append(city)
        if not city.is_adjacent_to(self):
            city._adjacent_cities.append(self)


    def remove_link_to_city(self, city):
        """Remove the link, in both sides, between the city and another city

        Args:
            city (City): the city to remove the link
        """
        if city in self._adjacent_cities:
            self._adjacent_cities.remove(city)
            city._adjacent_cities.remove(self)


    def remove_every_link(self):
        """Remove every link (in both sides) between the city and another cities
        """
        for city in self._adjacent_cities:
            city._adjacent_cities.remove(self)
        self._adjacent_cities = list()


    def remove_link_to_most_linked_city(self):
        """Remove the link (in both sides) between the city and one of its adjacent cities.
        The city from we cut the link is the adjacent city which has the more links to other cities.
        If other adjacent cities have the same number of links, the farthest is chosen.
        """
        max_adj = 0
        most_linked = -1
        l = 0
        for adj in self._adjacent_cities:
            nb_adj_adj = len(adj._adjacent_cities)
            if nb_adj_adj > max_adj:
                max_adj = nb_adj_adj
                most_linked = adj
                l = self.dist2_to_city(most_linked)
            elif nb_adj_adj == max_adj:
                l2 = self.dist2_to_city(most_linked)
                if l2 > l:
                    most_linked = adj
                    l = l2

        self.remove_link_to_city(most_linked)        


    def dist2_to_city(self, city):
        """Returns the square distance to another city

        Args:
            city (City): another city

        Returns:
            int: square distance
        """
        delta_x = self.x - city.x
        delta_y = self.y - city.y
        return (delta_x * delta_x + delta_y * delta_y)       

    
    def is_valid_capital_position(self):
        """Return true if the city has a link with at least 3 cities, 
        and is not adjacent to a capital.

        Returns:
            bool: can the city be a capital
        """
        if len(self._adjacent_cities) < 3:
            return False
        return not self.is_adjacent_with_capital()


    def is_adjacent_with_capital(self):
        """Return true if the city has a link to a capital.

        Returns:
            bool: is the city linked to a capital
        """
        for adj in self._adjacent_cities:
            if adj.is_capital:
                return True


    def sort_links_ccw(self):
        """Sorts adjacent (linked) cities to make them in counter clockwise order
        """
        if len(self._adjacent_cities) == 0:
            return

        c1 = self._adjacent_cities[0]
        angles_dict = {c1: 0}
        x1 = c1.x - self.x
        y1 = c1.y - self.y

        for c2 in self._adjacent_cities[1:]:
            x2 = c2.x - self.x
            y2 = c2.y - self.y
            angles_dict[c2] = math.atan2(y1,x1) - math.atan2(y2,x2)

        sorted_angles = dict(sorted(angles_dict.items(), key=lambda item: item[1]))
        self._adjacent_cities = list(sorted_angles.keys())

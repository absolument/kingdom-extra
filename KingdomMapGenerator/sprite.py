""" This module define a Sprite class and a LandscapeImage class.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

class Sprite:
    """A Sprite is defined by its source (sprite sheet), its widht, its height, 
    and its position in the sprite sheet
    """
    def __init__(self, id, params):
        self.id = id
        self.x_source, self.y_source, self.width, self.height = [params[i] for i in range(4)]
        if len(params) == 4:
            self.x_offset = 0
            self.y_offset = 0
        elif len(params) == 6:
            self.x_offset = params[4]            
            self.y_offset = params[5]            
        else:
            print("Wrong sprite parameters!")


class LandscapeImage:
    """A landscape image: a sprite with a position in the map
    """
    def __init__(self, sprite, x, y):
        """Create a landscape image

        Args:
            sprite (Sprite): the sprite to draw
            x (int): x position in pixels of the image
            y (int): y position in pixels of the image
        """
        self.sprite = sprite
        self.x = x
        self.y = y
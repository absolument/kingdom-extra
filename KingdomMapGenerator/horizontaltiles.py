"""
This module manages horizontal tiles generation. Little sprites (like trees for a forest)
are assembled in larger images that are tiled horizontally.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import math
import random

from sprite import LandscapeImage, Sprite


class PatternImage:
    """Class to assemble sprites in an image which will be used to build the tiles
    """
    def __init__(self):
        """Create and empty pattern"""
        self._images = list()
        self._centers = list()
        self._width = 0
        self._height = 0


    def fill(self, width, height, min_nb_img, max_nb_img, min_img_dist, sprites_source, sprite_manager, sprite_names):
        """Randomly build an image with several sprites.
        Number of sprites will be randomly choosen between min_nb_sprites and max_nb_sprites

        Args:
            width (int): width of the pattern image
            height (int): height of the pattern image
            min_nb_sprites (int): minimum number of sprites
            max_nb_sprites (int): maximum number of sprites
            min_sprites_dist (int): minimum distance in both x and y axis between the centers of two sprites
            sprites_source (string): key to identify the sprite sheet
            sprite_manager (SpriteManager)
            sprite_names (list of string): names of the sprites to randomly choose
        """
        self._width = width
        self._height = height
        self._min_img_dist = min_img_dist

        nb_img = random.randint(min_nb_img, max_nb_img)
        max_iterations = nb_img * 10
        id_img = 0
        id_iteration = 0

        while id_img < nb_img and id_iteration < max_iterations:
            sprite_name = random.choice(sprite_names)
            sprite = sprite_manager.get_sprite(sprites_source, sprite_name)
            x = random.randint(0, width - sprite.width - 1)
            y = random.randint(0, height - sprite.height - 1)
            x_center = x + sprite.width // 2
            y_center = y + sprite.height // 2

            if self._is_img_far_enought(x_center, y_center):
                self._images.append(LandscapeImage(sprite, x, y))
                self._centers.append([x_center, y_center])
                id_img += 1
            id_iteration += 1

        if id_iteration == max_iterations:
            print("Warning: maximum number of iterations exceeded in tile pattern!")
            print(f"(asked {nb_img} images, but only {id_img} have been defined.)")
            

    @property
    def images(self):
        """Get images (sprites with their positions)

        Returns:
            list of LandscapeImage: the images of the pattern
        """
        return self._images

    @property
    def width(self):
        """Get the width of the pattern image

        Returns:
            int: the width
        """
        return self._width

    @property
    def height(self):
        """get the heigth of the pattern image

        Returns:
            int: the heigth
        """
        return self._height

    def _is_img_far_enought(self, x_center, y_center):
        """Test if an image at (x_center,y_center) would be far enought from the other images

        Args:
            x_center (int): x coordinate of the center of the image
            y_center (int): y coordinate of the center of the image

        Returns:
            bool: is the image far enought
        """
        for center in self._centers:
            if abs(center[0] - x_center) < self._min_img_dist:
                return False
            if abs(center[1] - y_center) < self._min_img_dist:
                return False
        return True


class HorizontalTile:
    """Class to generate a single horizontal tile from two PatternImage
    """
    def __init__(self, pattern_left, pattern_right, tile_width):
        """Create a tile from two pattern images

        Args:
            pattern_left (PatternImage): images to add on the left of the tile
            pattern_right (PatternImage): images to add on the right of the tile
            tile_width (int): the width of the tile in pixels
        """
        half_width = tile_width // 2

        self._images = list()

        for image in pattern_left.images:
            x_image_left = image.x - pattern_left.width // 2
            if -tile_width <= x_image_left < tile_width:
                self._images.append(LandscapeImage(image.sprite, x_image_left, image.y))

        for image in pattern_right.images:
            x_image_right = image.x + tile_width - pattern_right.width // 2
            if -tile_width <= x_image_right < tile_width:
                self._images.append(LandscapeImage(image.sprite, x_image_right, image.y))

        self._images.sort(key = lambda image: image.y + image.sprite.height - image.sprite.y_offset)


    @property
    def images(self):
        """get images (sprites with their positions)

        Returns:
            list of LandscapeImage: the images of the tile
        """
        return self._images


class HorizontalTilesSet:
    """Class to generate and export a set of horizontal tiles
    """

    def __init__(self, htiles_dict, sprite_manager):
        """Generate a set of images that tiles horizontaly

        Args:
            htiles_dict (dict): tiles generation parameters
            sprite_manager (SpriteManager)
        """

        self._patterns = list()
        self._tiles = dict()
        self._tiles_position_in_spritesheet = dict()

        self._nb_patterns = htiles_dict["nb_patterns"]
        self._htiles_source = htiles_dict["htiles_source"]
        self._tile_width = htiles_dict["tile_width"]
        self._tile_height = htiles_dict["tile_height"]


        pattern_width = math.floor(self._tile_width * 1.9)
        pattern_height = self._tile_height
        self._sprites_source = htiles_dict["sprites_source"]
        sprite_names = htiles_dict["sprites"]

        min_img_by_pattern = htiles_dict["min_nb_img"]
        max_img_by_pattern = htiles_dict["max_nb_img"]
        min_img_dist = htiles_dict["min_img_dist"]
       
        for i in range(self._nb_patterns + 1):
            pattern_image = PatternImage()
            if i:
                pattern_image.fill(pattern_width, pattern_height, \
                    min_img_by_pattern, max_img_by_pattern, min_img_dist, \
                    self._sprites_source, sprite_manager, sprite_names)

            self._patterns.append(pattern_image)

        tile_id = 0
        for i, pattern_i in enumerate(self._patterns):
            for j, pattern_j in enumerate(self._patterns):
                if i != j:
                    self._tiles[(i,j)] = HorizontalTile(pattern_i, pattern_j, self._tile_width)
                    x = (tile_id % (self._nb_patterns + 1)) * self._tile_width
                    y = (tile_id // (self._nb_patterns + 1)) * self._tile_height
                    self._tiles_position_in_spritesheet[(i,j)] = (x,y)
                    tile_id += 1

        

    @property
    def htiles_source(self):
        """Get the name of the spritesheet composed with the tiles

        Returns:
            string: the name of the spritesheet
        """
        return self._htiles_source

    @property
    def sprites_source(self):
        """Get the name of the spritesheet where we take the sprites to compose tiles

        Returns:
            string: the name of the spritesheet
        """
        return self._sprites_source

    @property
    def tile_width(self):
        """Get the width of a tile

        Returns:
            int: tile width
        """
        return self._tile_width

    @property
    def tile_height(self):
        """Get the heigth of a tile

        Returns:
            int: tile height
        """
        return self._tile_height

    @property
    def nb_patterns(self):
        """Get the number of patterns in the tile set

        Returns:
            int: number of pattern
        """
        return self._nb_patterns

    @property
    def htiles_sheet_width(self):
        """Get the width of the tilesheet composed with the tiles

        Returns:
            int: tilesheet widht
        """
        return self.tile_width * (self._nb_patterns + 1)

    @property
    def htiles_sheet_height(self):
        """Get the height of the tilesheet composed with the tiles

        Returns:
            int: tilesheet height
        """
        return self.tile_height * self._nb_patterns


    def export_tiles_definition(self):
        """Export the definition of the tiles (list of sprites with their position)

        Returns:
            list: data to build the tiles
        """
        tiles_definition = dict()
        tiles_definition["source"] = self._sprites_source
        tiles_definition["tilesheet_width"] = (self._nb_patterns + 1) * self._tile_width
        tiles_definition["tilesheet_height"] = self._nb_patterns * self._tile_height
        tiles_definition["tile_width"] = self._tile_width
        tiles_definition["tile_height"] = self._tile_height
        tiles_definition["tiles"] = list()

        for key, tile in self._tiles.items():
            current_tile = dict()
            current_tile["x"] = self._tiles_position_in_spritesheet[key][0]
            current_tile["y"] = self._tiles_position_in_spritesheet[key][1]
            current_tile["sprites"] = list()
            for image in tile.images:
                x = image.x + self._tiles_position_in_spritesheet[key][0]
                y = image.y + self._tiles_position_in_spritesheet[key][1]
                current_tile["sprites"].append([image.sprite.id, x, y])
            tiles_definition["tiles"].append(current_tile)
        return tiles_definition


    def export_tiles_as_sprites(self):
        """Create Sprites from the tiles.
        Sprites names are tile_i_j where i and j are the number of the pattern.
        
        Returns:
            dict: keys are sprites names, values are Sprite
        """
        dict_sprites = dict()
        sprite_id = 0
        
        for key, tile in self._tiles.items():
            i,j = key
            sprite_key = "tile_" + str(i) + "_" + str(j)
            x_source = self._tiles_position_in_spritesheet[(i,j)][0]
            y_source = self._tiles_position_in_spritesheet[(i,j)][1]
            dict_sprites[sprite_key] = Sprite(sprite_id,[x_source, y_source, self._tile_width, self._tile_height])
            sprite_id += 1

        return dict_sprites







#!/usr/bin/env python3

"""
Kingdom map generator.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import mapanalysis
from kingdommap import KingdomMap
import kingdomsgen
import math
from shutil import copyfile


map_width = 4608
map_height = 3072

map_gen_params = {\
    "margin": 150, \
    "one_hour_dist": 100, \
    "minimum_distance": 1, \
    "maximum_distance": 3, \
    "initial_distance": 1.8, \
    "random_radius": 0.9, \
    "minimum_angle": math.pi / 5, \
    "language": "french"}

sprites_filename = "sprites.json"
landforms_filename = "landforms.json"



def main():

    kingdom_map = KingdomMap(map_width, map_height)

    kingdom_map.generate_graph(map_gen_params)
    #kingdom_map.load_graph_from_json('graph.json', map_gen_params["language"], map_gen_params["margin"])

    kingdom_map.generate_landscape(sprites_filename, landforms_filename)

    kingdom_map.export_graph("graph.json")
    kingdom_map.export_render_data("renderData.json")

    nb_capitals = len([city for city in kingdom_map.map_graph.cities if city.is_capital])

    kingdoms = kingdomsgen.KingdomsGenerator(kingdom_map.map_graph, nb_capitals * 3 // 4, 4)
    kingdoms.export_to_json('gameState.json')

    if not mapanalysis.check_links_consistency(kingdom_map.map_graph.cities):
        print("Error: inconsistent links in the map!")

    mapanalysis.print_statistics(kingdom_map.map_graph.cities)
    

if __name__ == "__main__":
    main()


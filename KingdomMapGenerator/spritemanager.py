""" This module define a SpriteManager class.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import json
from sprite import Sprite
from horizontaltiles import HorizontalTilesSet



class SpriteManager:
    """Class to manage the Sprites
    """
    def __init__(self, sprites_filename):
        """Create the manager and load the list of sprites from a json file

        Args:
            sprites_filename (string): path to the json file with sprites definition
        """
        self._tile_size = dict()
        self._sprites = dict()
        self._cities_sprites = dict()
        self._htiles_sets = dict()
        self._load_sprites(sprites_filename)
        

    def export_sprites_definition(self):
        """Export sprites definition in a dictionary
        """
        sprites_def = dict()
        for source, sprites_by_source in self._sprites.items():
            temp_sprites_dict = dict()
            for sprite in sprites_by_source.values():
                temp_sprites_dict[sprite.id] = [sprite.x_source, sprite.y_source, sprite.width, sprite.height]
            sprites_def[source] = [temp_sprites_dict[sprite_id] for sprite_id in sorted(temp_sprites_dict.keys())]
        return sprites_def

    def export_cities_sprites_definition(self):
        """Export sprites used for cities in a dictionary"""
        return self._cities_sprites


    def export_htiles_definition(self):
        """Export horizontal tiles definition in a dictionary
        """
        return {key:htiles_set.export_tiles_definition() for key,htiles_set in self._htiles_sets.items()}


    def get_htile_width(self, htiles_source):
        """Get the width of the tiles in an horizontal tiles set

        Args:
            htiles_source (string): the name of the horizontal tiles source

        Returns:
            int: the width of the tiles
        """
        return self._htiles_sets[htiles_source].tile_width


    def get_htile_height(self, htiles_source):
        """Get the height of the tiles in an horizontal tiles set

        Args:
            htiles_source (string): the name of the horizontal tiles source

        Returns:
            int: the height of the tiles
        """
        return self._htiles_sets[htiles_source].tile_height


    def get_htile_nb_patterns(self, htiles_sources):
        """Get the number of patterns in an horizontal tiles set
        Args:
            htiles_source (string): the name of the horizontal tiles source

        Returns:
            int: number of patterns
        """
        return self._htiles_sets[htiles_sources].nb_patterns


    def get_tile_size(self, sprite_source):
        """Get the size of a tiled sprite

        Args:
            sprite_source (string): key to identify the sprite sheet

        Returns:
            int: tile size
        """
        return self._tile_size[sprite_source]


    def get_sprite(self, sprite_source, sprite_name):
        """Get a sprite

        Args:
            sprite_source (string): key to identify the sprite sheet
            sprite_name (string): name of the sprite

        Returns:
            Sprite: the sprite we want
        """
        return self._sprites[sprite_source][sprite_name]


    def gen_htiles_set(self, htiles_dict):
        """Generate a set or horizontal tiles

        Args:
            htiles_dict (dict): horizontal tiles set parameters as defined in a json input file
        """
        htiles_set = HorizontalTilesSet(htiles_dict, self)
        self._htiles_sets[htiles_dict["htiles_source"]] = htiles_set
        self._sprites[htiles_dict["htiles_source"]] = htiles_set.export_tiles_as_sprites()


    def _load_sprites(self, sprites_filename):
        """Load sprites from a json file.

        Args:
            sprites_filename (string): path to the json file
        """
        with open(sprites_filename) as f:
            sprite_data = json.load(f)

        for sprite_family in sprite_data:

            if sprite_family["sprite_type"] == "cities":
                #we do not need sprites id for cities, we will just copy sprites parameters
                self._cities_sprites["source"] = sprite_family["source"]
                self._cities_sprites["sprites"] = sprite_family["sprites"]

            else:
                if not sprite_family["source"] in self._sprites:
                    self._sprites[sprite_family["source"]] = dict()
                current_id = len(self._sprites[sprite_family["source"]].keys())

                if sprite_family["sprite_type"] == "floating":
                    for sprite_name, sprite_params in sprite_family["sprites"].items():
                        self._sprites[sprite_family["source"]][sprite_name] = Sprite(current_id, sprite_params)
                        current_id += 1
                    
                elif sprite_family["sprite_type"] == "marching_square_tiles":
                    tile_size = sprite_family["tile_size"]
                    self._tile_size[sprite_family["source"]] = tile_size

                    for sprite_name, sprite_position in sprite_family["sprites"].items():
                        sprite_params = [ \
                            sprite_position[0] * tile_size, \
                            sprite_position[1] * tile_size, \
                            tile_size, tile_size]
                        self._sprites[sprite_family["source"]][sprite_name] = Sprite(current_id, sprite_params)
                        current_id += 1




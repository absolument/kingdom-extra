<?php
/**
 * World object
 */
class World {
	public $id;
	public $name;
	public $players = array();
	public $valid = false;
	
	private $format = "txt";
	private $stats_done = false;
	
	//stats
	public $pib = 0;
	public $glory = 0;
	public $age_moy = 0;
	public $age_med = 0;
	public $reputation = 0;
	
	
	/**
	 * @param int $mapID
	 * @param string $mapName
	 * @param string $format Optional, formated output
	 */
	public function __construct($mapID, $mapName, $format="txt") {
		$this->id =      intval($mapID);
		$this->name =    strval($mapName);
		$this->format = $format;
	}
	
	/**
	 * Add a player object
	 * @param Player $p
	 */
	public function addPlayer($p){
		$this->players[] = $p;
		$this->stats_done = false;
	}
	
	/**
	 * List players IDs
	 * @return string
	 */
	public function listPlayersIDs(){
		$ids = array();
		foreach($this->players as $p) $ids[] = $p->id;
		return $ids;
	}
	
	/**
	 * Compute some global stats
	 */
	public function stats(){
		$a = 0;
		$this->pib = 0;
		$this->glory = 0;
		$this->reputation = 0;
		foreach($this->players as $p){
			$a += $p->age;
			$this->pib += $p->commerce;
			$this->glory += $p->glory;
			$this->reputation += $p->reputation;
		}
		
		$nb = count($this->players);
		if($a > 0 && $nb > 0) {
			//Moyenne d'âge
			$this->age_moy = $a/$nb;
			//Age median
			if($nb%2 == 0){
				$this->age_med = ($this->players[floor($nb/2)]->age + $this->players[ceil($nb/2)]->age)/2;
			}
			else {
				$this->age_med = $this->players[floor($nb/2)]->age;
			}
		}
		$this->stats_done = true;
	}
	
	/**
	 * Format txt output
	 * @return string
	 */
	public function output_txt(){
		if(!$this->stats_done) $this->stats();
        return	sprintf("%- 4d %-20s|p:%'. 2d|a:%'. 5d,%'. 5d|c:%' 7d|g:%'. 5d|r:%'. 3d",
			$this->id, 
			$this->name,
			count($this->players),
			round($this->age_moy),
			round($this->age_med),
			$this->pib,
			$this->glory,
			$this->reputation
		);		
	}
	
	/**
	 * Format csv output (comma-separated)
	 * @return string
	 */
	public function output_csv(){
		if(!$this->stats_done) $this->stats();
        return	sprintf('%d,"%s",%d,%d,%d,%d,%d,%d',
			$this->id, 
			$this->name,
			count($this->players),
			round($this->age_moy,1),
			round($this->age_med,1),
			$this->pib,
			$this->glory,
			$this->reputation
		);		
	}
	
	/**
	 * Format js output
	 * @return string
	 */
	public function output(){
		if(!$this->stats_done) $this->stats();
		return sprintf(
			 "{\n"
			."  type: 'World',\n"
			."  id: %d,\n"
			."  name: '%s',\n"
			."  players: [%s],\n"
			."  age_moy: %d,\n"
			."  age_med: %d,\n"
			."  pib: %d,\n"
			."  glory: %d,\n"
			."  reputation: %d\n"
			."}\n",
			$this->id, 
			$this->name,
			implode(",", $this->listPlayersIDs()),
			round($this->age_moy),
			round($this->age_med),
			$this->pib,
			$this->glory,
			$this->reputation
		);
	}
	
	/**
	 * Convert to string
	 * @return string
	 */
	public function __toString()
    {
		if($this->format == "txt") return $this->output_txt();
		elseif($this->format == "csv") return $this->output_csv();
		else return $this->output();
    }
}